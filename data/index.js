var idDispositivo = "N/A";

function advance() {
  const btnAdvance = document.getElementById("swAdvance");
  const frmAdvance = document.getElementById("frmAdvance");
  const txtIp = document.getElementById("txtIp");
  const txtMask = document.getElementById("txtMask");
  const txtGtw = document.getElementById("txtGateway");
  frmAdvance.hidden = !btnAdvance.checked;
  if (btnAdvance.checked) {
    txtIp.value = "";
    txtMask.value = "";
    txtGtw.value = "";
  }
}

function closeFailedForm() {
  const failedForm = document.getElementById("failedForm");
  failedForm.hidden = true;
}

function RegisterModule() {
  const formLoading = document.getElementById("form-loading");
  formLoading.hidden = false;
  const rbWifi = document.getElementById("rbWifi");
  const txtSSID = document.getElementById("txtSSID");
  const txtPass = document.getElementById("txtPassword");
  const txtIp = document.getElementById("txtIp");
  const txtMask = document.getElementById("txtMask");
  const txtGtw = document.getElementById("txtGateway");

  const data = {
    typeConnection: rbWifi.checked ? "WiFi" : "Ethernet",
    ssid: txtSSID.value,
    password: txtPass.value,
    ip: txtIp.value,
    mask: txtMask.value,
    gtw: txtGtw.value,
  };

  fetch("/configure", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then(function (response) {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.text();
    })
    .then(function (data) {
      console.log("Dispositivo Configurado!");
      console.log(data);
      if (data == "OK") {
        window.location.replace("/Success");
      } else {
        window.location.replace("/Pending");
      }
      formLoading.hidden = true;
    })
    .catch(function (error) {
      console.error("Error:", error);
      formLoading.hidden = true;
    });
}

function testConnection() {
  const formLoading = document.getElementById("form-loading");
  formLoading.hidden = false;

  const rbWifi = document.getElementById("rbWifi");
  const txtSSID = document.getElementById("txtSSID");
  const txtPass = document.getElementById("txtPassword");
  const txtIp = document.getElementById("txtIp");
  const txtMask = document.getElementById("txtMask");
  const txtGtw = document.getElementById("txtGateway");

  if (!rbWifi || !txtSSID || !txtPass || !txtIp || !txtMask || !txtGtw) {
    console.error("One or more required elements not found");
    return;
  }

  const data = {
    typeConnection: rbWifi.checked ? "WiFi" : "Ethernet",
    ssid: txtSSID.value,
    password: txtPass.value,
    ip: txtIp.value,
    mask: txtMask.value,
    gtw: txtGtw.value,
  };

  fetch("/connect", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.text();
    })
    .then((data) => {
      try {
        const payload = JSON.parse(data);
        console.log(payload.status);
        console.log(payload.description);
        formLoading.hidden = true;
        const successForm = document.getElementById("successForm");
        const failedForm = document.getElementById("failedForm");
        if (payload.status == 1) {
          successForm.hidden = false;
          failedForm.hidden = true;
        } else {
          successForm.hidden = true;
          failedForm.hidden = false;
        }
      } catch (error) {
        console.error("Error parsing JSON response:", error);
        handleConnectionFailure();
      }
    })
    .catch((error) => {
      console.error("Error:", error);
      handleConnectionFailure();
    });

  function handleConnectionFailure() {
    const failedForm = document.getElementById("failedForm");
    failedForm.hidden = false;
    formLoading.hidden = true;
  }
}

function wifiEn() {
  const rbWifi = document.getElementById("rbWifi");
  const WifiForm = document.getElementById("WifiForm");
  const EthernetForm = document.getElementById("EthernetForm");
  const ConnectionType = document.getElementById("connection-type-title");

  if (rbWifi.checked) {
    ConnectionType.innerText = "Wifi";
    WifiForm.hidden = false;
    EthernetForm.hidden = true;
  } else {
    ConnectionType.innerText = "Ethernet";
    WifiForm.hidden = true;
    EthernetForm.hidden = false;
  }
}

function Scan() {
  const formLoading = document.getElementById("form-loading");
  const txtSSID = document.getElementById("txtSSID");
  const txtIdModulo = document.getElementById("txtIdModulo");
  const Eth = document.getElementById("EthState");
  formLoading.hidden = false;
  while (txtSSID.firstChild) {
    txtSSID.removeChild(txtSSID.firstChild);
  }
  fetch("/scan")
    .then(function (response) {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.text();
    })
    .then(function (data) {
      const payload = JSON.parse(data);
      idDispositivo = parseInt(payload.Id);
      txtIdModulo.textContent = idDispositivo;
      Eth.value = payload.ETH;
      payload.Wifi.forEach((element) => {
        const objSSID = document.createElement("option");
        objSSID.value = element.SSID;
        objSSID.text = `${element.SSID} [${processRSSI(element.RSSI)}]`;
        txtSSID.appendChild(objSSID);
      });
      formLoading.hidden = true;
    })
    .catch(function (error) {
      console.error("Error:", error);
      Eth.value = "Desconectado";
      formLoading.hidden = true;
    });
}

function processRSSI(rssi) {
  if (rssi > -40) {
    return "Excellet";
  } else if (rssi > -70) {
    return "Good";
  } else if (rssi > -80) {
    return "Fairly";
  } else {
    return "Weak";
  }
}

function ShowPassword() {
  const txtPass = document.getElementById("txtPassword");
  if (txtPass.type === "password") {
    txtPass.type = "text";
  } else {
    txtPass.type = "password";
  }
}

document.addEventListener("DOMContentLoaded", function () {
  Scan();
});
