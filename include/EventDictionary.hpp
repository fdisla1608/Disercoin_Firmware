struct EventDictionary {
    int number;
    const char* english;
    const char* spanish;
    // Agregar más idiomas según sea necesario
};

EventDictionary events[] = {
    {1, "Event 1", "Evento 1"},
    {2, "Event 2", "Evento 2"},
};