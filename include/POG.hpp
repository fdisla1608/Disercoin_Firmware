#pragma once

#include "Config.hpp"
#include "Module.hpp"
#include <Arduino.h>
#include <ArduinoJson.h>

namespace POG
{
/**
 * @brief Polls the events and actions to sent toPOG (Pot o' Gold) machine.
 *
 * @param printer_data Listen if the POG is printing.
 */
void POGPoll(String printer_data);

/**
 * @brief Sends coins to the POG machine.
 */
void sendCoinPOG();

/**
 * @brief Adds received coins to the total count for sent to POT o' GOLD.
 *
 * @param coin The number of coins received.
 * @return uint64_t The updated total count of coins received.
 */
uint64_t addToCoinReceive(uint16_t coin);

/**
 * @brief Sets the status of the POG module.
 *
 * @param type The type of status to set (0 for playing, 1 for paying).
 * @param value The value to set for the status.
 */
void setPOGStatus(int type, int value);

/**
 * @brief Retrieves the current status of the POG module.
 *
 * @return JsonDocument A JSON document containing the POG status.
 */
JsonDocument GetPOGStatus();

/**
 * @brief Sets the reset error state of the POG module.
 *
 * @param state The state to set (true for error, false for no error).
 */
void SetResetError(bool state);

/**
 * @brief Sets the daily ticket state of the POG module.
 *
 * @param state The state to set (true for daily ticket, false for no ticket).
 */
void SetDailyTicket(bool state);

/**
 * @brief Sets the cashout state of the POG module.
 *
 * @param state The state to set (true for cashout, false for no cashout).
 */
void SetCashout(bool state);

/**
 * @brief Sets the out of services state of the POG module.
 *
 * @param state The state to set (true for out of services, false for in service).
 */
void SetOutServices(bool state);

/**
 * @brief Blinks the LED for counting IN events.
 *
 * @param CounterInValue The value of the IN counter.
 * @return uint16_t The updated value of the IN counter.
 */
uint16_t BlinkLedIN(uint16_t CounterInValue);

/**
 * @brief Blinks the LED for counting OUT events.
 *
 * @param CounterOUTValue The value of the OUT counter.
 * @return uint16_t The updated value of the OUT counter.
 */
uint16_t BlinkLedOUT(uint16_t CounterOUTValue);

void SetPendingPay(uint16_t amount);
uint16_t GetPendingPay();

bool GetOutServices();

void SetProcessPay(bool state);
bool GetProcessPay();

} // namespace POG