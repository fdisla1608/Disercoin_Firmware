#pragma once

#include <ArduinoJson.h>
#include <Preferences.h>
#include <PCF8575.h>
#include <Adafruit_NeoPixel.h>

#include "Config.hpp"
#include "Network.hpp"

#define MAX_LOG_SIZE 600

namespace Module
{
    typedef struct
    {
        uint16_t cod;
        uint16_t data;
        uint32_t timeStamp;
    } Log;

    // INITIALIZATION
    /**
     * @brief Initializes the module.
     *
     * This function initializes all necessary variables and peripherals
     * for the module's operation.
     */
    void Initialize();

    // EEPROM PREFERENCES
    /**
     * @brief Initializes EEPROM preferences.
     *
     * This function initializes EEPROM and checks its status.
     */
    void InitEEPROMPreference();

    /**
     * @brief Writes the current configuration to EEPROM.
     *
     * This function writes the current module configuration to EEPROM for persistent storage.
     *
     * @param myModule The structure containing the current module configuration.
     * @return true if writing to EEPROM was successful.
     * @return false if an error occurred while writing to EEPROM.
     */
    bool WriteEEPROM(ModuleInfo myModule);

    /**
     * @brief Reads the module configuration from EEPROM.
     *
     * This function reads the module configuration from EEPROM.
     *
     * @return The module configuration read from EEPROM.
     */
    ModuleInfo ReadEEPROM();

    /**
     * @brief Configures the module based on the provided configuration structure.
     *
     * This function takes a structure containing module configuration information
     * and configures the module accordingly.
     *
     * @param myModuleConf The structure containing module configuration information.
     * @return true if the configuration process was successful.
     * @return false if an error occurred during the configuration process.
     */
    bool ConfigureModule(ModuleInfo myModuleConf);

    // LOGS
    /**
     * @brief Saves logs to EEPROM.
     *
     * This function saves module logs to EEPROM.
     *
     * @return The size of the data saved in EEPROM.
     */
    size_t SaveLog();

    /**
     * @brief Reads logs from EEPROM.
     *
     * This function reads module logs from EEPROM.
     *
     * @return The size of the data read from EEPROM.
     */
    size_t ReadLog();

    /**
     * @brief Updates the log record.
     *
     * This function updates the log record with a new entry.
     *
     * @param log The new log entry to add.
     */
    void UpdateLog(Log log);

    // MODULE INFORMATION
    /**
     * @brief Gets module information.
     *
     * This function retrieves the current module information in JSON format.
     *
     * @return A JSON document containing module information.
     */
    JsonDocument GetModuleInformation();

    /**
     * @brief Performs credit counting.
     *
     * This function performs credit counting.
     *
     * @param credits The type of credits to count.
     * @param amount The number of credits to add or subtract.
     * @return The total number of credits after the operation.
     */
    unsigned long CountingCredits(String credits, int amount);

    /**
     * @brief Gets the total credits.
     *
     * This function retrieves the total stored credits.
     *
     * @param credits The type of credits to query.
     * @return The total stored credits.
     */
    unsigned long GetCountingCredits(String credits);

    // PCF EVENT
    /**
     * @brief Reads the state of a PCF8575 digital pin.
     *
     * This function reads the state of a digital pin from PCF8575.
     *
     * @param pin The digital pin number to read.
     * @param debug Debug mode indicator.
     * @return The state of the digital pin.
     */
    uint8_t DigitalRead(uint8_t pin, bool debug);

    /**
     * @brief Writes a value to a PCF8575 digital pin.
     *
     * This function writes a value to a digital pin of PCF8575.
     *
     * @param source The source of the write.
     * @param pin The digital pin number to write.
     * @param value The value to write to the digital pin.
     * @param debug Debug mode indicator.
     */
    void DigitalWrite(String source, uint8_t pin, uint8_t value, bool debug);

    /**
     * @brief Reads the state of all digital pins of PCF8575.
     *
     * This function reads the state of all digital pins of PCF8575.
     *
     * @param debug Debug mode indicator.
     * @return A structure containing the state of all digital pins.
     */
    PCF8575::DigitalInput DigitalReadAll(bool debug);

    // UTILITY
    /**
     * @brief Sets the color of the NeoPixel.
     *
     * This function sets the color of the NeoPixel.
     *
     * @param color The color to set.
     */
    void setColor(uint32_t color);

    /**
     * @brief Gets the battery status.
     *
     * This function retrieves the battery status in JSON format.
     *
     * @return A JSON document containing the battery status.
     */
    JsonDocument GetBatteryStatus();

    /**
     * @brief Gets the LDR sensor status.
     *
     * This function retrieves the LDR sensor status in JSON format.
     *
     * @return A JSON document containing the LDR sensor status.
     */
    JsonDocument GetLdrStatus(void);

    // MONITORING
    /**
     * @brief Records a monitoring event.
     *
     * This function records a monitoring event in the monitoring system.
     *
     * @param cod The event code.
     * @param description The event description.
     * @param func The function that originated the event.
     * @param data The data associated with the event.
     */
    void MonitoringLog(uint8_t cod, String description, String func, uint16_t data);

    // POLLING
    /**
     * @brief Sets the module restart state.
     *
     * This function sets the module restart state.
     *
     * @param state The restart state to set.
     */
    void setRestartModule(bool state);

    /**
     * @brief Sets the module update state.
     *
     * This function sets the module update state.
     *
     * @param state The update state to set.
     */
    void setUpdateModule(bool state);

    /**
     * @brief Sets the print enable state.
     *
     * This function sets the print enable state.
     *
     * @param state The print enable state to set.
     */
    void setEnablePrint(bool state);

    /**
     * @brief Main loop of the module.
     *
     * This function represents the main loop of the module, where main tasks are performed.
     */
    void ModuleLOOP();
}