#pragma once

#include <Arduino.h>
#include <ArduinoJson.h>
#include <WiFi.h>
#include <ETH.h>
#include <HTTPUpdate.h>
#include <PubSubClient.h>
#include <ESPAsyncWebServer.h>
#include <DNSServer.h>
#include <SPIFFS.h>

#include "POG.hpp"

namespace Network
{
    /**
     * @brief Structure for HTTP response containing a response code and payload.
     */
    typedef struct
    {
        int responseCode; ///< The HTTP response code.
        String payload;   ///< The payload of the HTTP response.
    } HttpResponse;

    /**
     * @brief Initializes the module with the provided configuration.
     *
     * @param myModuleObj The configuration object for the module.
     */
    void Initialize(ModuleInfo myModuleObj);

    /**
     * @brief Handles the MQTT communication loop.
     */
    void MQTTLOOP();

    /**
     * @brief Callback function for MQTT messages.
     *
     * @param topic The topic of the received MQTT message.
     * @param payload The payload of the received MQTT message.
     * @param length The length of the payload.
     */
    void callback(char *topic, byte *payload, unsigned int length);

    /**
     * @brief Performs an OTA update.
     */
    void UpdateByOTA();

    /**
     * @brief Sends an amount of credits to the server.
     *
     * @param amount The amount of credits to send.
     * @param creditType The type of credit.
     */
    void POST_AMOUNT(int amount, int creditType);

    /**
     * @brief Sends initial information to the server.
     *
     * @param connectionType The type of connection (Ethernet or WiFi).
     */
    void PostInitialInformation(uint8_t connectionType);

    /**
     * @brief Sends data to the server for printing.
     *
     * @param printerData The data to be printed.
     */
    void PrintToServer(String printerData);

    /**
     * @brief Checks if the Ethernet connection is established.
     *
     * @return uint8_t Returns 1 if connected, 0 otherwise.
     */
    uint8_t ETHConnected();

    /**
     * @brief Checks if the MQTT connection is established.
     *
     * @return bool Returns true if connected, false otherwise.
     */
    bool MQTTConnected();

    /**
     * @brief Publishes a message to the MQTT broker.
     *
     * @param topic The topic to publish to.
     * @param payload The message payload.
     * @return bool Returns true if the message is published successfully, false otherwise.
     */
    bool MQTTPublish(const char *topic, const char *payload);

    /**
     * @brief Sends an HTTP POST request to the specified URL with the given payload.
     *
     * @param url The URL to send the POST request to.
     * @param payload The payload to include in the request.
     * @return HttpResponse The HTTP response object containing the response code and payload.
     */
    HttpResponse HTTPPOST(String url, String payload);

    /**
     * @brief Sends an HTTP GET request to the specified URL.
     *
     * @param url The URL to send the GET request to.
     * @return HttpResponse The HTTP response object containing the response code and payload.
     */
    HttpResponse HTTPGET(String url);

    /**
     * @brief Configures the web server routes and settings.
     */
    void configureWebServer();

    /**
     * @brief Handles the body of a connection request to the web server.
     *
     * @param request The HTTP request object.
     * @param data The request data.
     * @param len The length of the data.
     * @param index The index of the data.
     * @param total The total length of the data.
     */
    void onConnectBody(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total);

    /**
     * @brief Handles the body of a configuration request to the web server.
     *
     * @param request The HTTP request object.
     * @param data The request data.
     * @param len The length of the data.
     * @param index The index of the data.
     * @param total The total length of the data.
     */
    void onConfigureBody(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total);

    /**
     * @brief Handles requests for resources not found on the web server.
     *
     * @param request The HTTP request object.
     */
    void notFound(AsyncWebServerRequest *request);

    /**
     * @brief Processes the variables for web server template rendering.
     *
     * @param var The variable to process.
     * @return String The processed variable.
     */
    String processor(const String &var);

    /**
     * @brief Executes the web server loop for processing requests.
     */
    void WEBServerLoop();

}