#pragma once

#include <Arduino.h>
struct ModuleInfo
{
  String idModulo;
  String idDispositivo;
  String idBanca;
  int operationMode;
  int coinValue;
  String tipoModulo;
  String licencia;
  String versionFirmware;
  String WifiSSID;
  String WifiPassword;
  String typeConnection;
  int mqttPort;
  int httpPort;
  String staticIp;
  String subNetMask;
  String gateWay;
  String serverHost;
  String mqttHost;
  String mqttUserName;
  String mqttPassword;
  String mqttTopic;
  bool monitorEn;
  bool tkSerial;
  int counterIn;
  int counterOut;
};

typedef struct
{
  unsigned long counter;
  bool enable;
  bool counting;
  unsigned long last_update;
} pulses_t;

struct TicketInfo
{
  unsigned int id = 0;
  unsigned int amount = 0;
  bool state = true;
};

struct ErrState
{
  uint8_t id;
  uint32_t color;
  uint8_t tick;
  String detail;
  uint8_t tipo;
};

#define vFirmware "WT32-1.2.8"

#define INTERRUPT_PIN 4

#define I2S_SDA 14
#define I2S_SCL 15

#define SAS_RX 33
#define SAS_TX 32
#define SAS_ADDRESS 1

#define PRINT_TX 32
#define PRINT_RX 33

#define PIN_TTL_TX 17         // Green wire, Transmit Data Line from acceptor
#define PIN_TTL_RX 5          // Green wire, Transmit Data Line from acceptor
#define PIN_INTERRUPT_LINE 5  // Orange wire on Apex, Request to send data to host
#define PIN_SEND_LINE P15     // White/Blue wire, Host Ready Signal
#define PIN_ACEPTOR_ENABLE P7 // White/Blue wire, Host Ready Signal

#define STANDBYE_SERVER P0
#define IN_COUNTER P2
#define OUT_COUNTER P3

#define CREDITS_PLAYED P14
#define CREDITS_REGISTRED P13
#define CREDITS_EARNED P12
#define CREDITS_WITHDRAW P11

#define PIN_CREDITS P6
#define PIN_RESET P8
#define PIN_JACKPOT P9
#define PIN_SUMMARY P10

#define CHARGER_PIN 35
#define BATTERY_PIN 36
#define LDR_PIN 39
#define BTN_CONFIG P0
#define RGBPIN 2

#define ORANGE 0xFF3000
#define LED_OFF 0x000000
#define RED 0x7F0000
#define GREEN 0x007F00
#define BLUE 0x00007F
#define YELLOW 0xFFD700
#define WHITE 0x7F7F7F

const String SERVERHOST = "70.35.199.64";
const int SERVERPORT = 3000;

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <style>
      body {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        color: antiquewhite;
        font-family: Arial, Helvetica, sans-serif;
        background: #001177;
      }
      .welcome-container {
        display: flex;
        justify-content: space-between;
        flex-direction: column;
        width: 40vh;
        height: 20vh;
        background-color: white;
        border: 2px rgb(8, 8, 8) solid;
        border-radius: 10px;
        padding: 20px;
      }
      .welcome-title {
        width: 100%;
        text-align: center;
        font-size: 4.5vh;
        color: black;
      }

      .welcome-button--box {
        display: flex;
        justify-content: center;
      }

      .welcome-button {
        display: flex;
        justify-content: center;
        align-items: center;
        text-decoration: none;
        width: 30vw;
        height: 5vh;
        border: none;
        font-size: 2rem;
        color: white;
        background-color: RGB(25, 135, 84);
        border-radius: 5px;
      }

      .welcome-button:hover {
        color: RGB(25, 135, 84);
        background-color: white;
        border: black 1px solid;
      }
    </style>
  </head>
  <body>
    <div class="welcome-container">
      <div class="welcome-title">Trumore Module Configuration</div>
      <div class="welcome-button--box">
        <a href="/" class="welcome-button">Next</a>
      </div>
    </div>
  </body>
</html>
)rawliteral";