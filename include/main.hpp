#pragma once

// System
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_task_wdt.h>
#include <ETH.h>
#include <HTTPClient.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include "SPIFFS.h"
#include "PCF8575.h"

// Own-Lib
#include "Config.hpp"
#include "Module.hpp"

#define IP IPAddress(192, 168, 4, 1)
#define SubNetMask IPAddress(255, 255, 255, 0)

typedef enum
{
    OutServices = 0,
    Initializing = 1,
    Configuration = 2,
    Normal = 3,
} operation_mode;

void MainTask(void *arg);
void ReadingTask(void *arg);
void NET_event(WiFiEvent_t event);
boolean ConfigNetwork();

bool ConfigureModule(ModuleInfo myModuleConf);

void onConnectBody(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total);
void onConfigureBody(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total);
