#include "main.hpp"

// Init
TaskHandle_t ReadingTaskHandle;
TaskHandle_t MainTaskHandle;

HardwareSerial billSerial(1);
HardwareSerial printSerial(2);
pulses_t credits, withdraw, pay, play, notify;

uint16_t CounterInValue = 0;
uint16_t CounterOUTValue = 0;

uint32_t lastTimePrinterRead = 0;
String printer_data = "";
ModuleInfo myModule;
char logString[250];

bool isOut = false;

operation_mode operationMode;

unsigned long process_pay_timeout;

/**
 * @brief Main function to set up all the necessary components for the module to work.
 *
 */
void setup()
{
    Serial.begin(115200);
    printSerial.begin(2400, SERIAL_8N1, PRINT_RX, PRINT_TX);
    billSerial.begin(600, SERIAL_8N1, PIN_TTL_RX, PIN_TTL_TX);
    Serial.println("********************************************************************************************");
    Module::Initialize();
    Module::setColor(WHITE);
    operationMode = Initializing;
    myModule = Module::ReadEEPROM();
    Serial.printf("Firmware Version: %s, ID: %llu\n", vFirmware, ESP.getEfuseMac());
    Serial.printf("IdModulo: %s  IdDispositivo: %s \n", myModule.idModulo.c_str(), myModule.idDispositivo.c_str());
    Serial.println("********************************************************************************************");

    if (Module::DigitalRead(P0, false))
    {
        Module::setColor(BLUE);
        String ssid = "TR_Module_" + String(ESP.getEfuseMac());
        WiFi.mode(WIFI_AP_STA);
        WiFi.onEvent(NET_event);
        WiFi.softAP(ssid.c_str(), "");
        WiFi.softAPConfig(IP, IP, SubNetMask);

        Network::configureWebServer();
        sprintf(logString, "Module in Configuration Mode SSID: %s, Password: %s, Gateway: %s", ssid.c_str(), "",
                IP.toString());
        Module::MonitoringLog(0x1A, logString, "onWebServer", 0);
        operationMode = Configuration;
    }
    else
    {
        Module::MonitoringLog(0x30, "Modulo Trabajando en Modo Pulso", __func__, 0);
        Module::setColor(GREEN);
        operationMode = Normal;
        ConfigNetwork();
    }

    Module::DigitalWrite(__func__, IN_COUNTER, HIGH, false);
    Module::DigitalWrite(__func__, OUT_COUNTER, HIGH, false);

    xTaskCreatePinnedToCore(MainTask, "Main_Task", 8192, NULL, 2, &MainTaskHandle, 1);
    xTaskCreatePinnedToCore(ReadingTask, "Reading_Task", 8192, NULL, 5, &ReadingTaskHandle, 0);
}

void loop()
{
}

/**
 * @brief This task is for processing all the events in the module except for reading the pulse from Pot O' Gold.
 *
 * @param arg The argument passed to the task.
 */
void MainTask(void *arg)
{
    esp_task_wdt_init(10, false);
    while (true)
    {
        if (notify.enable)
        {
            Serial.println("Notifying....");
            JsonDocument info;
            String payload = "";
            info["idModule"] = myModule.idModulo;
            serializeJson(info, payload);
            Network::HttpResponse resp = Network::HTTPPOST("http://10.0.0.22:3002/api/notification", payload.c_str());
            notify.enable = false;
        }

        if ((millis() - play.last_update) > 10000)
        {
            POG::setPOGStatus(0, 0);
            play.enable = true;
        }
        if ((millis() - pay.last_update) > 4000)
        {
            POG::setPOGStatus(1, 0);
            pay.enable = true;
        }

        if ((credits.counter > 0) && ((millis() - credits.last_update) > 1500))
        {
            JsonDocument info;
            String payload = "";
            uint32_t buffer = credits.counter;
            String topic = "vs/modulos/" + myModule.idBanca + "/tpv";
            credits.counter = 0;
            info["idModulo"] = myModule.idModulo;
            info["Mensaje"] = "Venta Correcta";
            info["Codigo"] = 0;
            info["Monto"] = buffer;
            serializeJson(info, payload);
            Network::POST_AMOUNT(buffer, 1);
            Serial.println(topic.c_str());
            Serial.println(payload.c_str());
            Network::MQTTPublish(topic.c_str(), payload.c_str());
            POG::SetPendingPay(true);
            credits.counting = false;
        }

        if ((withdraw.counter > 0) && ((millis() - withdraw.last_update) > 1500))
        {
            JsonDocument info;
            String payload = "";
            uint32_t buffer = withdraw.counter;
            String topic = "vs/modulos/" + myModule.idBanca + "/tpv";
            withdraw.counter = 0;
            info["idModulo"] = myModule.idModulo;
            info["Mensaje"] = "Pago Pendiente";
            info["Codigo"] = 2;
            info["Monto"] = buffer;
            serializeJson(info, payload);
            Network::POST_AMOUNT(buffer, 2);
            Serial.println(topic.c_str());
            Serial.println(payload.c_str());

            POG::SetPendingPay(buffer);
            Network::MQTTPublish(topic.c_str(), payload.c_str());
            withdraw.counting = false;
        }

        if (printer_data.length() > 0 && ((millis() - lastTimePrinterRead) > 3000) && !withdraw.counting)
        {
            Module::MonitoringLog(0x81, "Printing Ticket", __func__, 0);
            Network::PrintToServer(printer_data.c_str());
            printer_data = "";
        }

        if (operationMode == Normal)
        {
            Network::MQTTLOOP();
            Module::ModuleLOOP();
            POG::POGPoll(printer_data);
        }
        else if (operationMode == Configuration)
        {
            Network::WEBServerLoop();
        }

        if (POG::GetPendingPay() > 0)
        {
            if (POG::GetProcessPay())
            {
                JsonDocument info;
                String payload = "";
                String topic = "vs/modulos/" + myModule.idBanca + "/tpv";
                info["idModulo"] = myModule.idModulo;
                info["Mensaje"] = "Pago Correcto";
                info["Codigo"] = 1;
                info["Monto"] = POG::GetPendingPay();
                serializeJson(info, payload);
                Serial.println(topic.c_str());
                Serial.println(payload.c_str());
                Network::MQTTPublish(topic.c_str(), payload.c_str());
                POG::SetPendingPay(0);
                POG::SetProcessPay(false);
                isOut = false;
                POG::SetResetError(true);
            }
            else if (millis() - process_pay_timeout > 30 * 1000 && !POG::GetOutServices() && !isOut)
            {
                POG::SetOutServices(true);
                process_pay_timeout = millis();
                isOut = true;
            }
        }
        else
        {
            process_pay_timeout = millis();
        }

        esp_task_wdt_reset();
    }
}

/**
 * @brief This task is especially to read all the pulses from the Pot O' Gold.
 *
 * @param arg The argument passed to the task.
 */
void ReadingTask(void *arg)
{
    esp_task_wdt_init(10, false);
    credits.enable = true;
    withdraw.enable = true;
    while (true)
    {
        esp_task_wdt_reset();

        PCF8575::DigitalInput pins = Module::DigitalReadAll(false);
        if (pins.p13 && credits.enable)
        {
            credits.last_update = millis();
            credits.enable = false;
            credits.counting = true;
            Module::DigitalWrite(__func__, IN_COUNTER, LOW, false);
        }
        else if (!pins.p13 && credits.enable == false)
        {
            credits.enable = true;
            uint32_t cmp = millis() - credits.last_update;
            Module::DigitalWrite(__func__, IN_COUNTER, HIGH, false);
            if (cmp < 350 && cmp > 10)
            {
                credits.counter++;
            }
            else
            {
                sprintf(logString, "Counter IN Bad Pulse Width: [%d]ms \n", cmp);
            }
        }

        if (pins.p11 && withdraw.enable)
        {
            withdraw.last_update = millis();
            withdraw.enable = false;
            withdraw.counting = true;
            Module::DigitalWrite(__func__, OUT_COUNTER, LOW, false);
        }
        else if (!pins.p11 && withdraw.enable == false)
        {
            withdraw.enable = true;
            uint32_t cmp = millis() - withdraw.last_update;
            Module::DigitalWrite(__func__, OUT_COUNTER, HIGH, false);
            if (cmp < 350 && cmp > 10)
            {
                withdraw.counter++;
            }
            else
            {
                sprintf(logString, "Counter OUT Bad Pulse Width:[%d]ms \n", cmp);
            }
        }

        if (pins.p14 && play.enable)
        {
            play.last_update = millis();
            play.counter = 1;
            play.enable = false;
            POG::setPOGStatus(0, 1);
            Module::DigitalWrite(__func__, IN_COUNTER, LOW, false);
        }
        else if (!pins.p14 && !play.enable)
        {
            play.enable = true;
            Module::DigitalWrite(__func__, IN_COUNTER, HIGH, false);
        }

        if (pins.p12 && pay.enable)
        {
            pay.last_update = millis();
            pay.enable = false;
            POG::setPOGStatus(1, 1);
            Module::DigitalWrite(__func__, OUT_COUNTER, LOW, false);
        }
        else if (!pins.p12 && !pay.enable)
        {
            pay.enable = true;
            Module::DigitalWrite(__func__, OUT_COUNTER, HIGH, false);
        }

        if (pins.p1 && !notify.enable && (millis() - notify.last_update) > 2000)
        {
            notify.last_update = millis();
            notify.enable = true;
        }

        char ch = printSerial.read();
        if ((ch > 31 && ch < 127) || ch == 10)
        {
            printer_data += ch;
            lastTimePrinterRead = millis();
        }
    }
}

/**
 * @brief Configures the network connection based on the module's specified type of connection (WiFi or Ethernet).
 *
 * @return true if the network connection is successfully established, false otherwise.
 */
boolean ConfigNetwork()
{
    if (myModule.typeConnection == "WiFi")
    {
        WiFi.mode(WIFI_STA);
        WiFi.onEvent(NET_event);
        WiFi.begin(myModule.WifiSSID.c_str(), myModule.WifiPassword.c_str());
        uint8_t wifiState = WiFi.waitForConnectResult(3000);
        if (wifiState == WL_CONNECTED)
        {
            return true;
        }
        else
        {
            Serial.println("Error to Connect Wifi");
            return false;
        }
    }
    else
    {
        if (!ETH.begin(ETH_PHY_ADDR, ETH_PHY_POWER))
        {
            Serial.println("Eth Connected");
            return true;
        }
        else
        {
            Serial.println("Error to Connect Ethernet");
            return false;
        }
    }
}

void NET_event(WiFiEvent_t event)
{
    char logString[250];
    switch (event)
    {
    case ARDUINO_EVENT_WIFI_STA_GOT_IP:
        Network::Initialize(myModule);
        Network::PostInitialInformation(event);
        sprintf(logString, "WiFi GOT IP: %s", WiFi.localIP().toString().c_str());
        Module::MonitoringLog(0x0B, logString, "Net Event", event);
        break;
    case ARDUINO_EVENT_WIFI_STA_DISCONNECTED:
        sprintf(logString, "Wifi Disconnected");
        Module::MonitoringLog(0x0B, logString, "Net Event", event);
        break;
    case ARDUINO_EVENT_ETH_DISCONNECTED:
        sprintf(logString, "Ethernet Disconnected");
        Module::MonitoringLog(0x0B, logString, "Net Event", event);
        break;
    case ARDUINO_EVENT_ETH_GOT_IP:
        sprintf(logString, "Ethernet GOT IP: %s", ETH.localIP().toString().c_str());
        Network::Initialize(myModule);
        Network::PostInitialInformation(event);
        Module::MonitoringLog(0x0B, logString, "Net Event", event);
        break;
    default:
        break;
    }
}
