#include <Module.hpp>

namespace Module
{

Preferences eEPROM;
PCF8575 pcf8575(0x20, I2S_SDA, I2S_SCL);
Adafruit_NeoPixel pixel(1, RGBPIN, NEO_GRB + NEO_KHZ800);
ModuleInfo myModule;

bool eEPROMState = false;
bool interruptState = false;
bool mqtt_initialized = false;

volatile bool flagUpdate = false;
volatile bool send_info_flag = false;
volatile bool out_of_service_flag = false;
volatile bool console_flag = false;

uint64_t lasTimeLogUpdate = 0;
Log logObj[MAX_LOG_SIZE];

bool updateModuleState = false, restartModuleState = false, enablePrintState = false;
PCF8575::DigitalInput pins;

void Initialize()
{
    pinMode(BATTERY_PIN, INPUT);
    pinMode(CHARGER_PIN, INPUT);
    pinMode(BATTERY_PIN, INPUT);

    // PCF8575
    pcf8575.pinMode(PIN_RESET, OUTPUT);
    pcf8575.pinMode(PIN_SUMMARY, OUTPUT);
    pcf8575.pinMode(PIN_CREDITS, OUTPUT);
    pcf8575.pinMode(PIN_JACKPOT, OUTPUT);

    // Envio al Pot o Gold
    pinMode(PIN_INTERRUPT_LINE, OUTPUT);

    // pinMode(PIN_TTL_TX, OUTPUT);
    pcf8575.pinMode(PIN_SEND_LINE, INPUT);
    pcf8575.pinMode(PIN_ACEPTOR_ENABLE, INPUT);

    // Botones para la configuracion
    pcf8575.pinMode(BTN_CONFIG, INPUT);
    pcf8575.pinMode(P1, INPUT);

    pcf8575.pinMode(CREDITS_REGISTRED, INPUT);
    pcf8575.pinMode(CREDITS_PLAYED, INPUT);
    pcf8575.pinMode(CREDITS_EARNED, INPUT);
    pcf8575.pinMode(CREDITS_WITHDRAW, INPUT);
    pcf8575.pinMode(IN_COUNTER, OUTPUT);
    pcf8575.pinMode(OUT_COUNTER, OUTPUT);
    pcf8575.begin();

    pcf8575.digitalWrite(PIN_RESET, LOW);
    pcf8575.digitalWrite(PIN_SUMMARY, LOW);
    pcf8575.digitalWrite(PIN_CREDITS, LOW);
    pcf8575.digitalWrite(PIN_JACKPOT, LOW);

    pcf8575.digitalWrite(IN_COUNTER, LOW);
    pcf8575.digitalWrite(OUT_COUNTER, LOW);

    pixel.begin();
    InitEEPROMPreference();
}

/**
 * @brief
 *
 */
void InitEEPROMPreference()
{
    if (eEPROM.begin("terminal"))
    {
        MonitoringLog(0x0A, "EEPROM Init Successfully", __func__, 0);
        eEPROMState = true;
    }
    else
    {
        MonitoringLog(0x60, "Errot to Init EEPROM", __func__, 0);
        eEPROMState = false;
    }
}

/**
 * @brief Configures the module based on the provided configuration structure.
 *
 * This function takes a structure containing module configuration information
 * and configures the module accordingly.
 *
 * @param myModuleConf The structure containing module configuration information.
 * @return true if the configuration process was successful.
 * @return false if an error occurred during the configuration process.
 */

bool ConfigureModule(ModuleInfo myModuleConf)
{
    char url[200];
    sprintf(url, "http://70.35.199.64:3000/modulos/config/%llu", ESP.getEfuseMac());
    JsonDocument doc;
    Network::HttpResponse response = Network::HTTPGET(String(url));
    if (response.payload.length() > 0 && response.responseCode == 200)
    {
        deserializeJson(doc, response.payload);
        myModuleConf.idModulo = doc["id_modulo"].as<String>();
        myModuleConf.idBanca = doc["idBanca"].as<String>();
        myModuleConf.operationMode = doc["protocoloModulo"];
        myModuleConf.coinValue = doc["ValorMoneda"];
        myModuleConf.mqttHost = doc["mqttServerIp"].as<String>();
        myModuleConf.mqttPort = doc["mqttServerPort"];
        myModuleConf.mqttUserName = doc["mqttUserName"].as<String>();
        myModuleConf.mqttPassword = doc["mqttPasswd"].as<String>();
        myModuleConf.mqttTopic = "trunmore/modulos";
        myModuleConf.counterIn = doc["centrada"];
        myModuleConf.counterOut = doc["csalida"];
        myModuleConf.serverHost = "70.35.199.64";
        myModuleConf.tkSerial = doc["ticketSerial"];
        myModuleConf.monitorEn = doc["monitorSerial"];
        myModuleConf.httpPort = 3000;
        Module::WriteEEPROM(myModuleConf);
        Module::MonitoringLog(0x1E, "Modulo Registrado Correctamente!", __func__, 0);
        return true;
    }
    else
    {
        Serial.printf("Response Code: %d", response.responseCode);
        Module::MonitoringLog(0x65, "Modulo No Registrado...", __func__, 0);
        return false;
    }
}

bool WriteEEPROM(ModuleInfo myModule)
{
    if (eEPROMState)
    {
        MonitoringLog(0x0B, "Writting on EEPROM.", __func__, 0);
        eEPROM.putString("idModulo", myModule.idModulo);
        eEPROM.putString("idDispositivo", myModule.idDispositivo);
        eEPROM.putString("idBanca", myModule.idBanca);
        eEPROM.putInt("operationMode", myModule.operationMode);
        eEPROM.putInt("coinValue", myModule.coinValue);
        eEPROM.putString("typeConnection", myModule.typeConnection);
        eEPROM.putString("staticIp", myModule.staticIp);
        eEPROM.putString("subNetMask", myModule.subNetMask);
        eEPROM.putString("gateWay", myModule.gateWay);
        eEPROM.putString("serverHost", myModule.serverHost);
        eEPROM.putInt("httpPort", myModule.httpPort);
        eEPROM.putInt("mqttPort", myModule.mqttPort);
        eEPROM.putString("mqttUserName", myModule.mqttUserName);
        eEPROM.putString("mqttPassword", myModule.mqttPassword);
        eEPROM.putString("mqttTopic", myModule.mqttTopic);
        eEPROM.putString("wifiSSID", myModule.WifiSSID);
        eEPROM.putString("wifiPassword", myModule.WifiPassword);
        eEPROM.putInt("counterIn", myModule.counterIn);
        eEPROM.putInt("counterOut", myModule.counterOut);
        eEPROM.putBool("tkSerial", myModule.tkSerial);
        eEPROM.putBool("monitorEn", myModule.monitorEn);
        MonitoringLog(0x0C, "EEPROM Wrote Successfuly", __func__, 0);
        return true;
    }
    else
    {
        return false;
    }
}

ModuleInfo ReadEEPROM()
{
    if (eEPROMState)
    {
        myModule.idModulo = eEPROM.getString("idModulo");
        myModule.idDispositivo = eEPROM.getString("idDispositivo");
        myModule.idBanca = eEPROM.getString("idBanca");
        myModule.tipoModulo = "WT32-ETH";
        myModule.operationMode = eEPROM.getInt("operationMode");
        myModule.coinValue = eEPROM.getInt("coinValue");
        myModule.typeConnection = eEPROM.getString("typeConnection");
        myModule.staticIp = eEPROM.getString("staticIp");
        myModule.subNetMask = eEPROM.getString("subNetMask");
        myModule.gateWay = eEPROM.getString("gateWay");
        myModule.serverHost = eEPROM.getString("serverHost");
        myModule.httpPort = eEPROM.getInt("httpPort");
        myModule.mqttPort = eEPROM.getInt("mqttPort");
        myModule.mqttUserName = eEPROM.getString("mqttUserName");
        myModule.mqttPassword = eEPROM.getString("mqttPassword");
        myModule.mqttTopic = eEPROM.getString("mqttTopic");
        myModule.WifiSSID = eEPROM.getString("wifiSSID");
        myModule.WifiPassword = eEPROM.getString("wifiPassword");
        myModule.counterIn = eEPROM.getInt("counterIn");
        myModule.counterOut = eEPROM.getInt("counterOut");
        myModule.licencia = "N/A";
        myModule.versionFirmware = vFirmware;
        myModule.mqttHost = "70.35.199.64";
        myModule.tkSerial = eEPROM.getBool("tkSerial");
        myModule.monitorEn = eEPROM.getBool("monitorEn");
    }
    return myModule;
}

JsonDocument GetModuleInformation()
{
    JsonDocument Module;
    Module["IdModulo"] = myModule.idModulo;
    Module["IdDispositivo"] = myModule.idDispositivo;
    Module["IdBanca"] = myModule.idBanca;
    Module["ModoOperacion"] = myModule.operationMode;
    Module["TipoModulo"] = myModule.tipoModulo;
    Module["Licencia"] = myModule.licencia;
    Module["Idioma"] = "ES";
    Module["VersionFirmware"] = myModule.versionFirmware;
    Module["TicketSerial"] = myModule.tkSerial;
    Module["MonitorEnable"] = myModule.monitorEn;
    return Module;
}

size_t SaveLog()
{
    if (eEPROMState)
    {
        size_t size = eEPROM.putBytes("log", logObj, sizeof(Log) * MAX_LOG_SIZE);
        if (size > 0)
        {
            // Module::MonitoringLog(0x0C, 1, "EEPROM Wrote Successfully", __func__);
            ReadLog();
        }
        else
        {
            Module::MonitoringLog(0x62, "Error to Write EEPROM", __func__, 0);
        }
        return size;
    }
    Module::MonitoringLog(0x62, "Error to Write EEPROM", __func__, 0);
    return 0;
}

size_t ReadLog()
{
    if (eEPROMState)
    {
        // Module::MonitoringLog(0x0D, 1, "EEPROM Reading Data", __func__);
        size_t size = eEPROM.getBytes("log", &logObj, sizeof(Log) * MAX_LOG_SIZE);
        if (size > 0)
        {
            // Module::MonitoringLog(0x0E, 1, "EEPROM Read Successfully", __func__);
        }
        else
        {
            Module::MonitoringLog(0x61, "Error to Read EEPROM", __func__, 0);
        }
        return size;
    }
    Module::MonitoringLog(0x61, "Error to Read EEPROM", __func__, 0);
    return 0;
}

unsigned long CountingCredits(String credits, int amount)
{
    if (eEPROMState)
    {
        unsigned long creditsAmount = eEPROM.getLong(credits.c_str()) + amount;
        if (eEPROM.putLong(credits.c_str(), creditsAmount) > 0)
        {
            return creditsAmount;
        }
        else
        {
            MonitoringLog(0x62, "Error to Write EEPROM", __func__, 0);
            return creditsAmount;
        }
    }
    else
    {
        MonitoringLog(0x61, "Error to Read EEPROM", __func__, 0);
        return 0;
    }
}

unsigned long GetCountingCredits(String credits)
{
    if (eEPROMState)
    {
        return eEPROM.getLong(credits.c_str());
    }
    else
    {
        MonitoringLog(0x61, "Error to Read EEPROM", __func__, 0);
        return 0;
    }
}

void DigitalWrite(String source, uint8_t pin, uint8_t value, bool debug)
{
    if (debug)
    {
        Serial.printf("%s - PCF Writting pin: %d to state: %d\n", source.c_str(), pin, value);
    }
    pcf8575.digitalWrite(pin, value);
}

uint8_t DigitalRead(uint8_t pin, bool debug)
{
    uint8_t pinState = pcf8575.digitalRead(pin);
    if (debug)
    {
        Serial.printf("PCF Reading pin: %d to state: %d\n", pin, pinState);
    }
    return pinState;
}

PCF8575::DigitalInput DigitalReadAll(bool debug)
{
    if (debug)
    {
        Serial.printf("PCF Reading all pin to state...\n");
    }
    return pcf8575.digitalReadAll();
}

void MonitoringLog(uint8_t cod, String description, String func, uint16_t data)
{
    boolean isPublished;
    JsonDocument doc;
    String payload = "";
    Log logt;

    doc["idModulo"] = myModule.idModulo;
    doc["Parameters"]["IdDispositivo"] = myModule.idDispositivo;
    doc["TipoLog"] = 1;

    logt.cod = cod;
    doc["Parameters"]["Log"]["Cod"] = cod;

    logt.data = data;
    doc["Parameters"]["Log"]["Data"] = data;
    doc["Parameters"]["Log"]["Description"] = description;
    serializeJson(doc, payload);
    if (Network::MQTTConnected())
    {
        isPublished = Network::MQTTPublish("trunmore/server/logs", payload.c_str());
    }
    UpdateLog(logt);
    Serial.printf("[LOG] %s \n", payload.c_str());
}

void UpdateLog(Log log)
{
    for (int i = MAX_LOG_SIZE - 1; i > 0; i--)
    {
        logObj[i] = logObj[i - 1];
    }
    logObj[0] = log;
    // Module::SaveLog();
}

JsonDocument GetLdrStatus(void)
{
    JsonDocument ldrJson;
    int LDRValue = analogRead(LDR_PIN);
    if (LDRValue > 1500)
    {
        ldrJson["Estado"] = 1;
    }
    else
    {
        ldrJson["Estado"] = 0;
    }
    return ldrJson;
}

JsonDocument GetBatteryStatus()
{
    JsonDocument batteryJson;
    int bvalue = (int)analogReadMilliVolts(BATTERY_PIN) / 1000;
    int percent = (int)map((bvalue * 1000), 0, 4300, 0, 1000) / 10;
    uint8_t cpinvalue = digitalRead(CHARGER_PIN);
    if (bvalue < 1 && cpinvalue)
    {
        batteryJson["Estado"] = 0; // Sin Bateria
    }
    else if (bvalue < 4 && cpinvalue)
    {
        batteryJson["Estado"] = 2; // Bateria Cargando
    }
    else if (!cpinvalue)
    {
        batteryJson["Estado"] = 1; // Solo Bateria
    }
    batteryJson["Voltaje"] = bvalue;
    batteryJson["Porcentaje"] = percent;
    return batteryJson;
}

void setColor(uint32_t color)
{
    pixel.clear();
    if (color > 0)
    {
        pixel.setPixelColor(0, color);
    }
    pixel.show();
}

void ModuleLOOP()
{
    if (updateModuleState)
    {
        Serial.println("Updatating Module");
        Network::UpdateByOTA();
    }
    else if (restartModuleState)
    {
        Serial.println("Restarting Module");
        ESP.restart();
    }
}

void setUpdateModule(bool state)
{
    updateModuleState = state;
}

void setRestartModule(bool state)
{
    restartModuleState = state;
}

void setEnablePrint(bool state)
{
    enablePrintState = state;
}

void console(const char *format, ...)
{
    va_list args;
    char s[250];
    va_start(args, format);
    vsprintf(s, format, args);
    va_end(args);
    Serial.print(s);
    if (console_flag)
    {
        Network::MQTTPublish("trunmore/consola", s);
    }
}

} // namespace Module