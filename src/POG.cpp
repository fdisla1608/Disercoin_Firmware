#include "POG.hpp"

namespace POG
{

int payingState = 0;
int playingState = 0;

unsigned long clockerON = 0;
unsigned long clockerOFF = 0;

unsigned long clocketON = 0;
unsigned long clocketOFF = 0;

unsigned long clockCoinHIGH = 0;
unsigned long clockCoinLOW = 0;

// POG EVENT
unsigned long resetErrorTimer = 0;
unsigned long dailyTicketTimer = 0;
unsigned long cashoutTimer = 0;

bool resetError = false;
bool outOfServices = false;
bool dailyTicket = false;
bool cashout = false;

uint16_t pending_pay;
uint16_t process_pay;

uint64_t total_coins_receive = 0;

void POGPoll(String printer_data)
{

    if (resetError)
    {
        Module::MonitoringLog(0x46, "Reseting the POG", __func__, 0);
        Module::DigitalWrite(__func__, PIN_RESET, HIGH, false);
        resetError = false;
        resetErrorTimer = millis();
    }
    else if ((millis() - resetErrorTimer) > 2000)
    {
        Module::DigitalWrite(__func__, PIN_RESET, LOW, false);
    }

    if (outOfServices)
    {
        Module::MonitoringLog(0x48, "Out of Services the POG", __func__, 0);
        Module::DigitalWrite(__func__, PIN_RESET, HIGH, false);
        delay(2000);
        Module::DigitalWrite(__func__, PIN_RESET, LOW, false);
        delay(2000);
        Module::DigitalWrite(__func__, PIN_RESET, HIGH, false);
        delay(8000);
        Module::DigitalWrite(__func__, PIN_RESET, LOW, false);
        Module::MonitoringLog(0x48, "Out of Services the POG", __func__, 0);
        outOfServices = false;
        resetErrorTimer = millis();
    }

    if (dailyTicket)
    {
        Module::MonitoringLog(0x46, "Preparing Daily Ticket", __func__, 0);
        Module::DigitalWrite(__func__, PIN_SUMMARY, HIGH, false);
        dailyTicket = false;
        dailyTicketTimer = millis();
        Module::setColor(WHITE);
    }
    else if ((millis() - dailyTicketTimer) > 2000 && printer_data.length() > 0)
    {
        Module::setColor(GREEN);
        Module::DigitalWrite(__func__, PIN_SUMMARY, LOW, false);
    }

    if (cashout)
    {
        Module::MonitoringLog(0x46, "Cashout Event", __func__, 0);
        Module::DigitalWrite(__func__, PIN_JACKPOT, HIGH, false);
        cashout = false;
        cashoutTimer = millis();
    }
    else if ((millis() - cashoutTimer) > 300)
    {
        Module::DigitalWrite(__func__, PIN_JACKPOT, LOW, false);
    }

    sendCoinPOG();
}

void sendCoinPOG()
{
    if (total_coins_receive > 0 && (millis() - clockCoinHIGH > 200))
    {
        Module::DigitalWrite(__func__, PIN_CREDITS, HIGH, false);
        clockCoinHIGH = millis();
        total_coins_receive--;
    }
    else if (millis() - clockCoinHIGH > 100)
    {
        Module::DigitalWrite(__func__, PIN_CREDITS, LOW, false);
        clockCoinLOW = millis();
    }
}

uint64_t addToCoinReceive(uint16_t coin)
{
    total_coins_receive += coin;
    return total_coins_receive;
}

void setPOGStatus(int type, int value)
{
    switch (type)
    {
    case 0:
        playingState = value;
        break;
    case 1:
        payingState = value;
        break;
    }
}

JsonDocument GetPOGStatus()
{
    JsonDocument pogJson;
    pogJson["Jugando"] = playingState;
    pogJson["Pagando"] = payingState;
    return pogJson;
}

void SetResetError(bool state)
{
    resetError = state;
}

void SetDailyTicket(bool state)
{
    dailyTicket = state;
}

void SetCashout(bool state)
{
    cashout = state;
}

void SetOutServices(bool state)
{
    outOfServices = state;
}

bool GetOutServices()
{
    return outOfServices;
}

void SetPendingPay(uint16_t amount)
{
    pending_pay = amount;
}

uint16_t GetPendingPay()
{
    return pending_pay;
}

void SetProcessPay(bool state)
{
    process_pay = state;
}

bool GetProcessPay()
{
    return process_pay;
}

uint16_t BlinkLedIN(uint16_t CounterInValue)
{
    if (CounterInValue > 0 && (millis() - clockerON) > 50)
    {
        Module::DigitalWrite(__func__, IN_COUNTER, LOW, false);
        CounterInValue--;
        clockerON = millis();
    }
    else if ((millis() - clockerOFF) > 50)
    {
        Module::DigitalWrite(__func__, IN_COUNTER, HIGH, false);
        clockerOFF = millis();
    }
    return CounterInValue;
}

uint16_t BlinkLedOUT(uint16_t CounterOUTValue)
{
    if (CounterOUTValue > 0 && (millis() - clocketON) > 50)
    {
        Module::DigitalWrite(__func__, OUT_COUNTER, LOW, false);
        CounterOUTValue--;
        clocketON = millis();
    }
    else if ((millis() - clocketOFF) > 50)
    {
        Module::DigitalWrite(__func__, OUT_COUNTER, HIGH, false);
        clocketOFF = millis();
    }
    return CounterOUTValue;
}

} // namespace POG