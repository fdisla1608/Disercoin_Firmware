var class_killswitch_allocator =
[
    [ "KillswitchAllocator", "class_killswitch_allocator.html#afc349d6629e6fa77643191c61d6019db", null ],
    [ "~KillswitchAllocator", "class_killswitch_allocator.html#ae370d5c8fde06787e8dd6c46f5fc6a74", null ],
    [ "allocate", "class_killswitch_allocator.html#a9a47d0e5bd7f6b8d2b11ff88b9e85242", null ],
    [ "deallocate", "class_killswitch_allocator.html#a6a642f26b25d656d8c8ad0762d07984c", null ],
    [ "on", "class_killswitch_allocator.html#af5a2f93105e0a24634af5793d2c5c5b8", null ],
    [ "reallocate", "class_killswitch_allocator.html#a523e1f7f9c51092169a94d9d0b34954d", null ]
];