var class_catch_1_1_option =
[
    [ "Option", "class_catch_1_1_option.html#aaab802d5beca2042f1be3949b3b18ad7", null ],
    [ "Option", "class_catch_1_1_option.html#a1957a689b0d040142db6c8e1e0a15a06", null ],
    [ "Option", "class_catch_1_1_option.html#a8b4a2a92a3fa093ece427cff4ca62cb4", null ],
    [ "~Option", "class_catch_1_1_option.html#a9be9e7c7ba1f32b29f556cd4b3b64954", null ],
    [ "none", "class_catch_1_1_option.html#a36c07062e9e9b7327d313b4ef410a18f", null ],
    [ "operator bool", "class_catch_1_1_option.html#a67b76affb3b5d35fa419ac234144038b", null ],
    [ "operator!", "class_catch_1_1_option.html#a61efd4196a96540ee018fee8791f3f10", null ],
    [ "operator*", "class_catch_1_1_option.html#a3f534efce0b043d603eb8f5653268e7d", null ],
    [ "operator*", "class_catch_1_1_option.html#a3325d1e02cdc82a508e0943f4ad53dfd", null ],
    [ "operator->", "class_catch_1_1_option.html#a4442b1b9eab40e7939e1e01cdedcb7ae", null ],
    [ "operator->", "class_catch_1_1_option.html#abaade5c780b539b94de660e314cb0c90", null ],
    [ "operator=", "class_catch_1_1_option.html#a542c56d7d1c5da3e3b4e464d6ba9571e", null ],
    [ "operator=", "class_catch_1_1_option.html#ac68e37d3a7d07c34a3ac7b4d0fba44a7", null ],
    [ "reset", "class_catch_1_1_option.html#ad20897c5c8bd47f5d4005989bead0e55", null ],
    [ "some", "class_catch_1_1_option.html#a5666e6f87d49eba31136710ead0f6e59", null ],
    [ "valueOr", "class_catch_1_1_option.html#a9bc53df8f74067e5f80d434898e6ddd4", null ]
];