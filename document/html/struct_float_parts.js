var struct_float_parts =
[
    [ "FloatParts", "struct_float_parts.html#a9b8593c9ab17e93e8656ab04dcb35e87", null ],
    [ "decimal", "struct_float_parts.html#a2c0400d24d9ed130d1b4b1b5e140dc89", null ],
    [ "decimalPlaces", "struct_float_parts.html#ad5d8215183446d765a0624cb6c900eaf", null ],
    [ "exponent", "struct_float_parts.html#a91b08ab679361f37d05c5acc352dd25d", null ],
    [ "integral", "struct_float_parts.html#a93bec9360b18532bea30bfdeb36a109e", null ]
];