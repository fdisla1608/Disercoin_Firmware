var class_string =
[
    [ "String", "class_string.html#a1ca860ade3727599dd71400ab34fcf1f", null ],
    [ "String", "class_string.html#a88ffc95cc8ac550f8d44c30a88d00372", null ],
    [ "c_str", "class_string.html#a03d29d324464cf785ad3f3e51469a5dc", null ],
    [ "concat", "class_string.html#a0917225836cdc9bec224946d16391212", null ],
    [ "concat", "class_string.html#ae9c804046088f6b762d86ca638ade484", null ],
    [ "length", "class_string.html#a71beef478599935add531a7866ad9908", null ],
    [ "limitCapacityTo", "class_string.html#af61b6fef7ec94804255524e13f9fc420", null ],
    [ "operator=", "class_string.html#afbb170bf08b55cb07545b3996e0f85bc", null ],
    [ "operator==", "class_string.html#a980d3634731950256ff4fcf1c905adfd", null ],
    [ "operator[]", "class_string.html#a27cf05c8999b3dd17f643440be716ec0", null ],
    [ "operator<<", "class_string.html#a3e28756ef05f6d718c9ab4a1e635c685", null ]
];