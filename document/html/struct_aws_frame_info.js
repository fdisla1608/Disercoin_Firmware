var struct_aws_frame_info =
[
    [ "final", "struct_aws_frame_info.html#a4f8368af5c2a76681d3e2e2c7cb4e075", null ],
    [ "index", "struct_aws_frame_info.html#a3e39e0381ab73d88040027129d7555bb", null ],
    [ "len", "struct_aws_frame_info.html#accdb2dff228b7a3ea4c3f61937a82412", null ],
    [ "mask", "struct_aws_frame_info.html#ada50f0e7b89816d02459e3e520b018bf", null ],
    [ "masked", "struct_aws_frame_info.html#aa2d28197b3553ab55a99af230d948ca4", null ],
    [ "message_opcode", "struct_aws_frame_info.html#a30c30e7510753f2f9e1836da1d2ad020", null ],
    [ "num", "struct_aws_frame_info.html#a7beea8f6745c478347f244cadef771c2", null ],
    [ "opcode", "struct_aws_frame_info.html#a5c1b56e6bccc2a95dbddf1a08e56e87d", null ]
];