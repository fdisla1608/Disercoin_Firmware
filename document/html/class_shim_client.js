var class_shim_client =
[
    [ "ShimClient", "class_shim_client.html#ad7d6f15706ca77be2846bdb423ffac65", null ],
    [ "available", "class_shim_client.html#a4549a76725f2e4c013e4d57018366109", null ],
    [ "connect", "class_shim_client.html#a6b82591ed734e454c1c5c242650c6e2d", null ],
    [ "connect", "class_shim_client.html#acfb86d1c9e7ead418b2d3ca06d74424f", null ],
    [ "connected", "class_shim_client.html#a9e380772318e83a4b9280fdf6db5e8bb", null ],
    [ "error", "class_shim_client.html#a91004c239942d94aa16e9d9dc0f45160", null ],
    [ "expect", "class_shim_client.html#a7d626f93ca043aabaff2c163bab3cf35", null ],
    [ "expectConnect", "class_shim_client.html#a15903dc22ed7eb35d9f2e90a44371e8e", null ],
    [ "expectConnect", "class_shim_client.html#a75ddaa8bee07e00006025b812072451b", null ],
    [ "flush", "class_shim_client.html#adac116554b543b7c4228c018a85882f5", null ],
    [ "operator bool", "class_shim_client.html#a9b3baad8c612d81b96e46f84d7e97580", null ],
    [ "peek", "class_shim_client.html#a9040fa1d479d71edf3a826f4691c35c4", null ],
    [ "read", "class_shim_client.html#aaab5dab5b969a87f538242e524431637", null ],
    [ "read", "class_shim_client.html#ac6937845219f4daa14b38f64ddce6366", null ],
    [ "received", "class_shim_client.html#a1362088e2254221897812548036b76ba", null ],
    [ "respond", "class_shim_client.html#ae03b7c1ce1df08956ef0fd67280a72d2", null ],
    [ "setAllowConnect", "class_shim_client.html#a339c5fc6dd3838d7586ea1c365179899", null ],
    [ "setConnected", "class_shim_client.html#a40223180afb122f19852e8cfd7ac31f0", null ],
    [ "stop", "class_shim_client.html#a8c528baf37154d347366083f0f816846", null ],
    [ "write", "class_shim_client.html#adf16c3bdf28db7016c114755d3b6389f", null ],
    [ "write", "class_shim_client.html#a4921dd89e0f14a9b77dfb3d7960401ca", null ]
];