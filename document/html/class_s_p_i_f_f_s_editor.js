var class_s_p_i_f_f_s_editor =
[
    [ "SPIFFSEditor", "class_s_p_i_f_f_s_editor.html#a9c79631acf794bf1ea3a69931b37f905", null ],
    [ "canHandle", "class_s_p_i_f_f_s_editor.html#a880537abf7da2a7b4fde569157d4ec7b", null ],
    [ "handleRequest", "class_s_p_i_f_f_s_editor.html#ab2f347aa983074d9c99032b3aea39b52", null ],
    [ "handleUpload", "class_s_p_i_f_f_s_editor.html#a2fe2eb6d627f275115b47fa82985e028", null ],
    [ "isRequestHandlerTrivial", "class_s_p_i_f_f_s_editor.html#a441034f73026f47b1f3123ef862363f0", null ]
];