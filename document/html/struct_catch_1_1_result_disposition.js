var struct_catch_1_1_result_disposition =
[
    [ "Flags", "struct_catch_1_1_result_disposition.html#aa705cf7e79a21c2352b00ffe20cd295f", [
      [ "Normal", "struct_catch_1_1_result_disposition.html#aa705cf7e79a21c2352b00ffe20cd295fa5ecbcf0afce98b042f35ec71ba03fa4b", null ],
      [ "ContinueOnFailure", "struct_catch_1_1_result_disposition.html#aa705cf7e79a21c2352b00ffe20cd295fa760881b8dc5a350fe1f65aefd9b3b9c6", null ],
      [ "FalseTest", "struct_catch_1_1_result_disposition.html#aa705cf7e79a21c2352b00ffe20cd295faa8934af5e371d5eaf8cf64dd7bf62238", null ],
      [ "SuppressFail", "struct_catch_1_1_result_disposition.html#aa705cf7e79a21c2352b00ffe20cd295faeeee77bd0a11e3907eb2b4f8ac92ab84", null ]
    ] ]
];