var struct_catch_1_1_test_case_info =
[
    [ "SpecialProperties", "struct_catch_1_1_test_case_info.html#a3e916b348b505647f565bf6cc4aa2f3e", [
      [ "None", "struct_catch_1_1_test_case_info.html#a3e916b348b505647f565bf6cc4aa2f3eac9d3e887722f2bc482bcca9d41c512af", null ],
      [ "IsHidden", "struct_catch_1_1_test_case_info.html#a3e916b348b505647f565bf6cc4aa2f3ea0013f99b933e634f0afc9259daa48e37", null ],
      [ "ShouldFail", "struct_catch_1_1_test_case_info.html#a3e916b348b505647f565bf6cc4aa2f3ea2ed226d6bb82816e43101969e721876c", null ],
      [ "MayFail", "struct_catch_1_1_test_case_info.html#a3e916b348b505647f565bf6cc4aa2f3ea4eafc4c7044b692b18e64fd113850316", null ],
      [ "Throws", "struct_catch_1_1_test_case_info.html#a3e916b348b505647f565bf6cc4aa2f3ea2007aa7ffff7adbe8260719b6e22bca3", null ],
      [ "NonPortable", "struct_catch_1_1_test_case_info.html#a3e916b348b505647f565bf6cc4aa2f3eac6e098ec8847f0df2d8ad35c6aab32d1", null ],
      [ "Benchmark", "struct_catch_1_1_test_case_info.html#a3e916b348b505647f565bf6cc4aa2f3ea939f08513d0c3666bc2c966265b27bd2", null ]
    ] ],
    [ "TestCaseInfo", "struct_catch_1_1_test_case_info.html#aaeb8d7a39be1f988ae50208213ed4d9b", null ],
    [ "expectedToFail", "struct_catch_1_1_test_case_info.html#aba4bddd0635617aec213220cb0dc4efc", null ],
    [ "isHidden", "struct_catch_1_1_test_case_info.html#a451227147204d105046094b758ce3438", null ],
    [ "okToFail", "struct_catch_1_1_test_case_info.html#ac3aa75b50d4ebea163c0998a58340aa9", null ],
    [ "tagsAsString", "struct_catch_1_1_test_case_info.html#a3309a1792f21bc1b4ead377c62930155", null ],
    [ "throws", "struct_catch_1_1_test_case_info.html#acde624e2423968a0bbe30eaefee32594", null ],
    [ "setTags", "struct_catch_1_1_test_case_info.html#a0fe44abaf18ae7c26f98a9fc2b08679c", null ],
    [ "className", "struct_catch_1_1_test_case_info.html#a7a53726c8d2e28a1e948ed39b24ed607", null ],
    [ "description", "struct_catch_1_1_test_case_info.html#a2e1454f6988673f814408646edaeb320", null ],
    [ "lcaseTags", "struct_catch_1_1_test_case_info.html#a1f3a8835a359f5dd8a103d6f1719fcde", null ],
    [ "lineInfo", "struct_catch_1_1_test_case_info.html#a859bf84dc0c6bd55bc794d80cb3fe452", null ],
    [ "name", "struct_catch_1_1_test_case_info.html#a9b45b3e13bd9167aab02e17e08916231", null ],
    [ "properties", "struct_catch_1_1_test_case_info.html#a0192e2df5fb2ea133eab670967efe364", null ],
    [ "tags", "struct_catch_1_1_test_case_info.html#a4b709da72927167a1ab690264eebe28c", null ]
];