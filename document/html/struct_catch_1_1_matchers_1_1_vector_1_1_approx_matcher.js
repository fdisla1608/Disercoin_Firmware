var struct_catch_1_1_matchers_1_1_vector_1_1_approx_matcher =
[
    [ "ApproxMatcher", "struct_catch_1_1_matchers_1_1_vector_1_1_approx_matcher.html#aec688a2254a71850c8f73e053f6eafe2", null ],
    [ "describe", "struct_catch_1_1_matchers_1_1_vector_1_1_approx_matcher.html#af5a363d69d226d3a5c8b248ab5c53583", null ],
    [ "epsilon", "struct_catch_1_1_matchers_1_1_vector_1_1_approx_matcher.html#a004797f3404adc4d8a5c06c59f5d08cc", null ],
    [ "margin", "struct_catch_1_1_matchers_1_1_vector_1_1_approx_matcher.html#a6cfd8948b4e29318b939ea4cb5b3a1a2", null ],
    [ "match", "struct_catch_1_1_matchers_1_1_vector_1_1_approx_matcher.html#a446e168063f096d38d81862d47640ed5", null ],
    [ "scale", "struct_catch_1_1_matchers_1_1_vector_1_1_approx_matcher.html#aa65f7b0635cb728a27644e254031e95c", null ],
    [ "approx", "struct_catch_1_1_matchers_1_1_vector_1_1_approx_matcher.html#afbde462b8fc7642912262328d47651a6", null ],
    [ "m_comparator", "struct_catch_1_1_matchers_1_1_vector_1_1_approx_matcher.html#a3d1adb59bc5a18ea00a4e28cbf58151f", null ]
];