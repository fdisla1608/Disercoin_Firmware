var namespace_catch =
[
    [ "Detail", "namespace_catch_1_1_detail.html", "namespace_catch_1_1_detail" ],
    [ "detail", "namespace_catch_1_1detail.html", "namespace_catch_1_1detail" ],
    [ "Generators", "namespace_catch_1_1_generators.html", "namespace_catch_1_1_generators" ],
    [ "literals", "namespace_catch_1_1literals.html", [
      [ "operator\"\"_a", "namespace_catch_1_1literals.html#a6e0ba8d5769ab9beea523100096e7b4f", null ],
      [ "operator\"\"_a", "namespace_catch_1_1literals.html#a1a48683b2f5d8d26b4eb952f98652fd1", null ]
    ] ],
    [ "Matchers", "namespace_catch_1_1_matchers.html", "namespace_catch_1_1_matchers" ],
    [ "always_false", "struct_catch_1_1always__false.html", null ],
    [ "AssertionHandler", "class_catch_1_1_assertion_handler.html", "class_catch_1_1_assertion_handler" ],
    [ "AssertionInfo", "struct_catch_1_1_assertion_info.html", "struct_catch_1_1_assertion_info" ],
    [ "AssertionReaction", "struct_catch_1_1_assertion_reaction.html", "struct_catch_1_1_assertion_reaction" ],
    [ "AutoReg", "struct_catch_1_1_auto_reg.html", "struct_catch_1_1_auto_reg" ],
    [ "BinaryExpr", "class_catch_1_1_binary_expr.html", "class_catch_1_1_binary_expr" ],
    [ "Capturer", "class_catch_1_1_capturer.html", "class_catch_1_1_capturer" ],
    [ "CaseSensitive", "struct_catch_1_1_case_sensitive.html", "struct_catch_1_1_case_sensitive" ],
    [ "Counts", "struct_catch_1_1_counts.html", "struct_catch_1_1_counts" ],
    [ "Decomposer", "struct_catch_1_1_decomposer.html", "struct_catch_1_1_decomposer" ],
    [ "ExceptionTranslatorRegistrar", "class_catch_1_1_exception_translator_registrar.html", "class_catch_1_1_exception_translator_registrar" ],
    [ "ExprLhs", "class_catch_1_1_expr_lhs.html", "class_catch_1_1_expr_lhs" ],
    [ "GeneratorException", "class_catch_1_1_generator_exception.html", "class_catch_1_1_generator_exception" ],
    [ "IConfig", "struct_catch_1_1_i_config.html", "struct_catch_1_1_i_config" ],
    [ "IContext", "struct_catch_1_1_i_context.html", "struct_catch_1_1_i_context" ],
    [ "IExceptionTranslator", "struct_catch_1_1_i_exception_translator.html", "struct_catch_1_1_i_exception_translator" ],
    [ "IExceptionTranslatorRegistry", "struct_catch_1_1_i_exception_translator_registry.html", "struct_catch_1_1_i_exception_translator_registry" ],
    [ "IGeneratorTracker", "struct_catch_1_1_i_generator_tracker.html", "struct_catch_1_1_i_generator_tracker" ],
    [ "IMutableContext", "struct_catch_1_1_i_mutable_context.html", "struct_catch_1_1_i_mutable_context" ],
    [ "IMutableEnumValuesRegistry", "struct_catch_1_1_i_mutable_enum_values_registry.html", "struct_catch_1_1_i_mutable_enum_values_registry" ],
    [ "IMutableRegistryHub", "struct_catch_1_1_i_mutable_registry_hub.html", "struct_catch_1_1_i_mutable_registry_hub" ],
    [ "IRegistryHub", "struct_catch_1_1_i_registry_hub.html", "struct_catch_1_1_i_registry_hub" ],
    [ "IResultCapture", "struct_catch_1_1_i_result_capture.html", "struct_catch_1_1_i_result_capture" ],
    [ "IRunner", "struct_catch_1_1_i_runner.html", "struct_catch_1_1_i_runner" ],
    [ "is_callable", "struct_catch_1_1is__callable.html", null ],
    [ "is_callable< Fun(Args...)>", "struct_catch_1_1is__callable_3_01_fun_07_args_8_8_8_08_4.html", null ],
    [ "is_callable_tester", "struct_catch_1_1is__callable__tester.html", null ],
    [ "is_range", "struct_catch_1_1is__range.html", null ],
    [ "IStream", "struct_catch_1_1_i_stream.html", "struct_catch_1_1_i_stream" ],
    [ "ITestCaseRegistry", "struct_catch_1_1_i_test_case_registry.html", "struct_catch_1_1_i_test_case_registry" ],
    [ "ITestInvoker", "struct_catch_1_1_i_test_invoker.html", "struct_catch_1_1_i_test_invoker" ],
    [ "ITransientExpression", "struct_catch_1_1_i_transient_expression.html", "struct_catch_1_1_i_transient_expression" ],
    [ "LazyExpression", "class_catch_1_1_lazy_expression.html", "class_catch_1_1_lazy_expression" ],
    [ "MatchExpr", "class_catch_1_1_match_expr.html", "class_catch_1_1_match_expr" ],
    [ "MessageBuilder", "struct_catch_1_1_message_builder.html", "struct_catch_1_1_message_builder" ],
    [ "MessageInfo", "struct_catch_1_1_message_info.html", "struct_catch_1_1_message_info" ],
    [ "MessageStream", "struct_catch_1_1_message_stream.html", "struct_catch_1_1_message_stream" ],
    [ "NameAndTags", "struct_catch_1_1_name_and_tags.html", "struct_catch_1_1_name_and_tags" ],
    [ "NonCopyable", "class_catch_1_1_non_copyable.html", "class_catch_1_1_non_copyable" ],
    [ "Option", "class_catch_1_1_option.html", "class_catch_1_1_option" ],
    [ "pluralise", "struct_catch_1_1pluralise.html", "struct_catch_1_1pluralise" ],
    [ "RegistrarForTagAliases", "struct_catch_1_1_registrar_for_tag_aliases.html", "struct_catch_1_1_registrar_for_tag_aliases" ],
    [ "ResultDisposition", "struct_catch_1_1_result_disposition.html", "struct_catch_1_1_result_disposition" ],
    [ "ResultWas", "struct_catch_1_1_result_was.html", "struct_catch_1_1_result_was" ],
    [ "ReusableStringStream", "class_catch_1_1_reusable_string_stream.html", "class_catch_1_1_reusable_string_stream" ],
    [ "RunTests", "struct_catch_1_1_run_tests.html", "struct_catch_1_1_run_tests" ],
    [ "ScopedMessage", "class_catch_1_1_scoped_message.html", "class_catch_1_1_scoped_message" ],
    [ "Section", "class_catch_1_1_section.html", "class_catch_1_1_section" ],
    [ "SectionEndInfo", "struct_catch_1_1_section_end_info.html", "struct_catch_1_1_section_end_info" ],
    [ "SectionInfo", "struct_catch_1_1_section_info.html", "struct_catch_1_1_section_info" ],
    [ "ShowDurations", "struct_catch_1_1_show_durations.html", "struct_catch_1_1_show_durations" ],
    [ "SimplePcg32", "class_catch_1_1_simple_pcg32.html", "class_catch_1_1_simple_pcg32" ],
    [ "SourceLineInfo", "struct_catch_1_1_source_line_info.html", "struct_catch_1_1_source_line_info" ],
    [ "StreamEndStop", "struct_catch_1_1_stream_end_stop.html", "struct_catch_1_1_stream_end_stop" ],
    [ "StringMaker", "struct_catch_1_1_string_maker.html", null ],
    [ "StringMaker< bool >", "struct_catch_1_1_string_maker_3_01bool_01_4.html", null ],
    [ "StringMaker< Catch::Detail::Approx >", "struct_catch_1_1_string_maker_3_01_catch_1_1_detail_1_1_approx_01_4.html", null ],
    [ "StringMaker< char * >", "struct_catch_1_1_string_maker_3_01char_01_5_01_4.html", null ],
    [ "StringMaker< char >", "struct_catch_1_1_string_maker_3_01char_01_4.html", null ],
    [ "StringMaker< char const * >", "struct_catch_1_1_string_maker_3_01char_01const_01_5_01_4.html", null ],
    [ "StringMaker< char[SZ]>", "struct_catch_1_1_string_maker_3_01char_0f_s_z_0e_4.html", null ],
    [ "StringMaker< double >", "struct_catch_1_1_string_maker_3_01double_01_4.html", null ],
    [ "StringMaker< float >", "struct_catch_1_1_string_maker_3_01float_01_4.html", null ],
    [ "StringMaker< int >", "struct_catch_1_1_string_maker_3_01int_01_4.html", null ],
    [ "StringMaker< long >", "struct_catch_1_1_string_maker_3_01long_01_4.html", null ],
    [ "StringMaker< long long >", "struct_catch_1_1_string_maker_3_01long_01long_01_4.html", null ],
    [ "StringMaker< R C::* >", "struct_catch_1_1_string_maker_3_01_r_01_c_1_1_5_01_4.html", null ],
    [ "StringMaker< R, typename std::enable_if< is_range< R >::value &&!::Catch::Detail::IsStreamInsertable< R >::value >::type >", "struct_catch_1_1_string_maker_3_01_r_00_01typename_01std_1_1enable__if_3_01is__range_3_01_r_01_4536d8fedfff6d62432b3dc59b56e1380.html", null ],
    [ "StringMaker< signed char >", "struct_catch_1_1_string_maker_3_01signed_01char_01_4.html", null ],
    [ "StringMaker< signed char[SZ]>", "struct_catch_1_1_string_maker_3_01signed_01char_0f_s_z_0e_4.html", null ],
    [ "StringMaker< std::nullptr_t >", "struct_catch_1_1_string_maker_3_01std_1_1nullptr__t_01_4.html", null ],
    [ "StringMaker< std::string >", "struct_catch_1_1_string_maker_3_01std_1_1string_01_4.html", null ],
    [ "StringMaker< std::wstring >", "struct_catch_1_1_string_maker_3_01std_1_1wstring_01_4.html", null ],
    [ "StringMaker< T * >", "struct_catch_1_1_string_maker_3_01_t_01_5_01_4.html", null ],
    [ "StringMaker< T[SZ]>", "struct_catch_1_1_string_maker_3_01_t_0f_s_z_0e_4.html", null ],
    [ "StringMaker< unsigned char >", "struct_catch_1_1_string_maker_3_01unsigned_01char_01_4.html", null ],
    [ "StringMaker< unsigned char[SZ]>", "struct_catch_1_1_string_maker_3_01unsigned_01char_0f_s_z_0e_4.html", null ],
    [ "StringMaker< unsigned int >", "struct_catch_1_1_string_maker_3_01unsigned_01int_01_4.html", null ],
    [ "StringMaker< unsigned long >", "struct_catch_1_1_string_maker_3_01unsigned_01long_01_4.html", null ],
    [ "StringMaker< unsigned long long >", "struct_catch_1_1_string_maker_3_01unsigned_01long_01long_01_4.html", null ],
    [ "StringMaker< wchar_t * >", "struct_catch_1_1_string_maker_3_01wchar__t_01_5_01_4.html", null ],
    [ "StringMaker< wchar_t const * >", "struct_catch_1_1_string_maker_3_01wchar__t_01const_01_5_01_4.html", null ],
    [ "StringRef", "class_catch_1_1_string_ref.html", "class_catch_1_1_string_ref" ],
    [ "TestCase", "class_catch_1_1_test_case.html", "class_catch_1_1_test_case" ],
    [ "TestCaseInfo", "struct_catch_1_1_test_case_info.html", "struct_catch_1_1_test_case_info" ],
    [ "TestFailureException", "struct_catch_1_1_test_failure_exception.html", null ],
    [ "TestInvokerAsMethod", "class_catch_1_1_test_invoker_as_method.html", "class_catch_1_1_test_invoker_as_method" ],
    [ "Timer", "class_catch_1_1_timer.html", "class_catch_1_1_timer" ],
    [ "Totals", "struct_catch_1_1_totals.html", "struct_catch_1_1_totals" ],
    [ "true_given", "struct_catch_1_1true__given.html", null ],
    [ "UnaryExpr", "class_catch_1_1_unary_expr.html", "class_catch_1_1_unary_expr" ],
    [ "UseColour", "struct_catch_1_1_use_colour.html", "struct_catch_1_1_use_colour" ],
    [ "WaitForKeypress", "struct_catch_1_1_wait_for_keypress.html", "struct_catch_1_1_wait_for_keypress" ],
    [ "WarnAbout", "struct_catch_1_1_warn_about.html", "struct_catch_1_1_warn_about" ],
    [ "exceptionTranslateFunction", "namespace_catch.html#a6f900893104846a2e846ed3bc2b2b9ed", null ],
    [ "ExceptionTranslators", "namespace_catch.html#a7e5614918a5ff0e4ac145a07cc58f7eb", null ],
    [ "FunctionReturnType", "namespace_catch.html#af1464ea71b2cb268ec7eb70e23c99cba", null ],
    [ "IConfigPtr", "namespace_catch.html#a0da1a24cde8bf4b44d1c9a4d5a9baf79", null ],
    [ "IReporterFactoryPtr", "namespace_catch.html#a08ecb3357829bc8dfdcba1e0075dfc0f", null ],
    [ "StringMatcher", "namespace_catch.html#a58f73243d23ff7af8760b3f9f064a204", null ],
    [ "Verbosity", "namespace_catch.html#abf3be10d03894afb391f3a2935e3b313", [
      [ "Quiet", "namespace_catch.html#abf3be10d03894afb391f3a2935e3b313a098753f8980036f4b936e3d4b6997111", null ],
      [ "Normal", "namespace_catch.html#abf3be10d03894afb391f3a2935e3b313a960b44c579bc2f6818d2daaf9e4c16f0", null ],
      [ "High", "namespace_catch.html#abf3be10d03894afb391f3a2935e3b313a655d20c1ca69519ca647684edbb2db35", null ]
    ] ],
    [ "cerr", "namespace_catch.html#afaf33e7ca0ff21482d1fe6ac689b02b0", null ],
    [ "cleanUp", "namespace_catch.html#ae900b054a1598c54acaa67902f87185d", null ],
    [ "cleanUpContext", "namespace_catch.html#ac07cdb7d744cc8f09672d924324b55fd", null ],
    [ "clog", "namespace_catch.html#ae16f6324853210e93116795ecf6d92a0", null ],
    [ "compareEqual", "namespace_catch.html#a77973c0a6eb296a04ed2100eaea5e5ab", null ],
    [ "compareEqual", "namespace_catch.html#a11cb906a2ad1dfc2d4b0cbdc1812ea37", null ],
    [ "compareEqual", "namespace_catch.html#af7a15abe1754f6de5b9c8949f7f9d729", null ],
    [ "compareEqual", "namespace_catch.html#a95d157dc92c10fddae5b64b8505c2d1d", null ],
    [ "compareEqual", "namespace_catch.html#abaa88cf0804653a77a89e51b1c4916e1", null ],
    [ "compareNotEqual", "namespace_catch.html#a4a994b5c04e84ac1ff4501bfb3ae1e43", null ],
    [ "compareNotEqual", "namespace_catch.html#ac84e35d8fbd63ae23f554ae1f5cc6c39", null ],
    [ "compareNotEqual", "namespace_catch.html#a01a4d58bc37aaeac87269d2e49006478", null ],
    [ "compareNotEqual", "namespace_catch.html#a00a7b0de0a93f0ea9fb74811b6a65b19", null ],
    [ "compareNotEqual", "namespace_catch.html#a5c92de0c4019c3c0528672065b9ad510", null ],
    [ "contains", "namespace_catch.html#ae39b54535a9b500016b1cefef99b3e5f", null ],
    [ "cout", "namespace_catch.html#aab6d5e77060f5c7936bd50d4210a8945", null ],
    [ "endsWith", "namespace_catch.html#a4911a430834b439495f16305df9196be", null ],
    [ "endsWith", "namespace_catch.html#a7e64e2d90cb7a3f1ed41b7ec348d923f", null ],
    [ "filterTests", "namespace_catch.html#ac30cf42d658b9e1fc3669a14c47a4b4c", null ],
    [ "formatReconstructedExpression", "namespace_catch.html#ab7a43c5fc95040e73f921d695f006f47", null ],
    [ "getAllTestCasesSorted", "namespace_catch.html#ab4ca52955527b075c98b2680b1966be6", null ],
    [ "getCurrentContext", "namespace_catch.html#aaa70096cfb925ec83319fd91be067219", null ],
    [ "getCurrentMutableContext", "namespace_catch.html#ad22507c2e4bc58f80a205db9756b8e29", null ],
    [ "getCurrentNanosecondsSinceEpoch", "namespace_catch.html#a734ecea6396d76457132d93ddbd8f790", null ],
    [ "getEstimatedClockResolution", "namespace_catch.html#ae061c00e97aff95706e05b8e78a6ce2b", null ],
    [ "getMutableRegistryHub", "namespace_catch.html#af3e57f259febe8bfc326dfb05b749308", null ],
    [ "getRegistryHub", "namespace_catch.html#aeda02bdcf49b680651e5eb8d2afb9cf1", null ],
    [ "getResultCapture", "namespace_catch.html#a650c5bc1188f0e6e88282010ec4376e4", null ],
    [ "handleExceptionMatchExpr", "namespace_catch.html#acf3102714ecc9a2a97e6a2dfd1e6d72b", null ],
    [ "handleExceptionMatchExpr", "namespace_catch.html#a4b3192e4c80b8b06d9347e6af937f3d5", null ],
    [ "handleExpression", "namespace_catch.html#af83b070d19f775ae946f9dbce3655e0d", null ],
    [ "handleExpression", "namespace_catch.html#a5699f2f67d4ac9c6506e0e3f64fd9c01", null ],
    [ "isFalseTest", "namespace_catch.html#a57094c243bbdbc524ead9509e7fc3ce6", null ],
    [ "isJustInfo", "namespace_catch.html#aa60628dd4cfcbaac1f5c7bc0cd6aeab1", null ],
    [ "isOk", "namespace_catch.html#accf3b043a3718473dcdfffdd87c33fe8", null ],
    [ "isThrowSafe", "namespace_catch.html#a81cce1176d63041ed0138e8033d7dd7d", null ],
    [ "makeMatchExpr", "namespace_catch.html#a044f554c458ff7495bea005efe46226e", null ],
    [ "makeStream", "namespace_catch.html#a0c3b96f79b7089b9505939e964c1ba5b", null ],
    [ "makeTestCase", "namespace_catch.html#a12396c2b59d2be16d2fa2270feb8d255", null ],
    [ "makeTestInvoker", "namespace_catch.html#a723eb2cfa2f21fe17831e2e16a51aa29", null ],
    [ "makeTestInvoker", "namespace_catch.html#aa6037fdfbe37bd5819055f91731b77ba", null ],
    [ "matchTest", "namespace_catch.html#aa90b1123bd506961484ee559125ccd87", null ],
    [ "operator\"\"_sr", "namespace_catch.html#a2dbdcbc7d47ce2c45423b25a909d2ad5", null ],
    [ "operator+", "namespace_catch.html#a6b27bb90cb48c758ecf9ec8d46bb7c6d", null ],
    [ "operator+=", "namespace_catch.html#a7bef8314a2073dcf229744d7a7c561e3", null ],
    [ "operator<<", "namespace_catch.html#a179dd69b090dd3391a122be88c46e8d0", null ],
    [ "operator<<", "namespace_catch.html#a8b5ca808579908929bb14cdf9e88dad2", null ],
    [ "operator|", "namespace_catch.html#aee9895c5da8f99c2e6889b65192eae1d", null ],
    [ "rangeToString", "namespace_catch.html#a64a7dbf9170ad10cad88e67249ffe5ab", null ],
    [ "rangeToString", "namespace_catch.html#a169cf020beeddf4d6e9cafbee7127f8a", null ],
    [ "replaceInPlace", "namespace_catch.html#a25efcf24924ff7c3777034ffb1ee5884", null ],
    [ "rng", "namespace_catch.html#a8ec43e1151a71d759292eff8f6f5594e", null ],
    [ "rngSeed", "namespace_catch.html#a17880285a46994a714df574bf93669e0", null ],
    [ "shouldContinueOnFailure", "namespace_catch.html#acf1118c0fb9dc8f90bde396df98f6d04", null ],
    [ "shouldSuppressFailure", "namespace_catch.html#a5606ddc745c7fde79f877bc51e85b754", null ],
    [ "splitStringRef", "namespace_catch.html#ac4e52f025013d4c3e59efd65c0380ce9", null ],
    [ "startsWith", "namespace_catch.html#a426f34f5c61d2e0193635d1fecc55d88", null ],
    [ "startsWith", "namespace_catch.html#aa6e19325aa3b050e57c743fd506d093a", null ],
    [ "throw_domain_error", "namespace_catch.html#a3f1a843bab3d30a07b17a5c70105109c", null ],
    [ "throw_exception", "namespace_catch.html#a311450ec327561c56c4fba4657ab687e", null ],
    [ "throw_logic_error", "namespace_catch.html#a9fc953d371b5cc75fe74a7d5b9a57c1b", null ],
    [ "throw_runtime_error", "namespace_catch.html#abbe0e2de86bd3322896827f878b19754", null ],
    [ "toLower", "namespace_catch.html#acdc9fc48dfe9bcb46e99e9c25477b04c", null ],
    [ "toLowerInPlace", "namespace_catch.html#adf12eacc2c95326e66fa5070d8773863", null ],
    [ "translateActiveException", "namespace_catch.html#a919e67998e52f2fe6244c84b393b65ae", null ],
    [ "trim", "namespace_catch.html#a64f9e69e92c102fb4213e1fbc54b3106", null ],
    [ "trim", "namespace_catch.html#a46e01b8eb16ddfd5a411a4ebb4e09571", null ]
];