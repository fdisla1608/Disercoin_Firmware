var struct_catch_1_1_i_mutable_context =
[
    [ "~IMutableContext", "struct_catch_1_1_i_mutable_context.html#a3aeecaecd2f575bf37336c3af4c6208f", null ],
    [ "setConfig", "struct_catch_1_1_i_mutable_context.html#aa6e3ef402c1e9f7c8ba27a8f34a77c92", null ],
    [ "setResultCapture", "struct_catch_1_1_i_mutable_context.html#a884a721adc78829aa290993875a03964", null ],
    [ "setRunner", "struct_catch_1_1_i_mutable_context.html#a300f5efc6b1fcd52681c6cf778df586e", null ],
    [ "cleanUpContext", "struct_catch_1_1_i_mutable_context.html#ac07cdb7d744cc8f09672d924324b55fd", null ],
    [ "getCurrentMutableContext", "struct_catch_1_1_i_mutable_context.html#ad22507c2e4bc58f80a205db9756b8e29", null ]
];