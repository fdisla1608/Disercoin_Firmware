var struct_custom_allocator =
[
    [ "CustomAllocator", "struct_custom_allocator.html#a7ea713017e02aa5f010e86c8b2eaefb8", null ],
    [ "allocate", "struct_custom_allocator.html#a47a6e9c6a62378881ae1ad8891a3abb8", null ],
    [ "deallocate", "struct_custom_allocator.html#a7293d440176162b417b81b45d2504e62", null ],
    [ "reallocate", "struct_custom_allocator.html#a305284b0619cf2d7a1f18c6bfd0cb1da", null ]
];