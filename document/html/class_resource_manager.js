var class_resource_manager =
[
    [ "ResourceManager", "class_resource_manager.html#a420ebb817c835293a64a949b8be2a084", null ],
    [ "~ResourceManager", "class_resource_manager.html#adcd84dbcdcbee123679a41a770054d52", null ],
    [ "ResourceManager", "class_resource_manager.html#abe96be7a072e74363ab934ff351b7994", null ],
    [ "allocator", "class_resource_manager.html#a2658c49ac82a3f4529eb7cfb2847a2ed", null ],
    [ "allocSlot", "class_resource_manager.html#ac8c0cff77af800cef6e8d5651773b5c8", null ],
    [ "clear", "class_resource_manager.html#ac8bb3912a3ce86b15842e79d0b421204", null ],
    [ "createString", "class_resource_manager.html#a82f14ca0ae14abfd7d2ac1d79e1a42b5", null ],
    [ "dereferenceString", "class_resource_manager.html#ac11f12603a61de95ce54d156081f6c1a", null ],
    [ "destroyString", "class_resource_manager.html#ad62fd2d001bfc37ab2c34769e0325eda", null ],
    [ "freeSlot", "class_resource_manager.html#a0dc1ebfa37a5670d0650b13f5bfa64eb", null ],
    [ "getSlot", "class_resource_manager.html#a12a314e4e43afb3146f8836d1ecbede8", null ],
    [ "getString", "class_resource_manager.html#a97215b7641c98d1d5acecefba8abc870", null ],
    [ "operator=", "class_resource_manager.html#a836f3677526b3b8aef0739bdb51590ab", null ],
    [ "overflowed", "class_resource_manager.html#a2c149df368c9c5969b7ea04d6a190784", null ],
    [ "resizeString", "class_resource_manager.html#a3e5daf50cd36c7d0850d8c85fbf81a34", null ],
    [ "saveString", "class_resource_manager.html#a9640d5629a3907236469ce81da6428d0", null ],
    [ "saveString", "class_resource_manager.html#a7d3f560f9c8a814c153a3ef3339bc39a", null ],
    [ "shrinkToFit", "class_resource_manager.html#a84fb9ead4f0f7458491b5d310592cea9", null ],
    [ "size", "class_resource_manager.html#a259cb5a711406a8c3e5d937eb9350cca", null ],
    [ "swap", "class_resource_manager.html#a5453ca8daac29d0fa313d9d5fd7ca1cc", null ]
];