var _ram_reader_8hpp =
[
    [ "IsCharOrVoid< T >", "struct_is_char_or_void.html", null ],
    [ "IsCharOrVoid< const T >", "struct_is_char_or_void_3_01const_01_t_01_4.html", null ],
    [ "Reader< TSource *, typename enable_if< IsCharOrVoid< TSource >::value >::type >", "struct_reader_3_01_t_source_01_5_00_01typename_01enable__if_3_01_is_char_or_void_3_01_t_source_01_4_1_1value_01_4_1_1type_01_4.html", "struct_reader_3_01_t_source_01_5_00_01typename_01enable__if_3_01_is_char_or_void_3_01_t_source_01_4_1_1value_01_4_1_1type_01_4" ],
    [ "BoundedReader< TSource *, typename enable_if< IsCharOrVoid< TSource >::value >::type >", "struct_bounded_reader_3_01_t_source_01_5_00_01typename_01enable__if_3_01_is_char_or_void_3_01_t_9b7dc667778463d5ab18e09c707348b4.html", "struct_bounded_reader_3_01_t_source_01_5_00_01typename_01enable__if_3_01_is_char_or_void_3_01_t_9b7dc667778463d5ab18e09c707348b4" ]
];