var class_armored_allocator =
[
    [ "~ArmoredAllocator", "class_armored_allocator.html#ada41514b40fff2aa552ec9cca89d18e9", null ],
    [ "allocate", "class_armored_allocator.html#af853011c126dabd96378e3c5b37a8d1a", null ],
    [ "deallocate", "class_armored_allocator.html#a156f09eb5b2910f624b2874dca95c81d", null ],
    [ "reallocate", "class_armored_allocator.html#a667f95cc41d062b211abcaf06490e283", null ]
];