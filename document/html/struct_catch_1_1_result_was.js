var struct_catch_1_1_result_was =
[
    [ "OfType", "struct_catch_1_1_result_was.html#a3df655c4352b2a8f892113ec2324ebba", [
      [ "Unknown", "struct_catch_1_1_result_was.html#a3df655c4352b2a8f892113ec2324ebbaa4e81c184ac3ad48a389cd4454c4a05bb", null ],
      [ "Ok", "struct_catch_1_1_result_was.html#a3df655c4352b2a8f892113ec2324ebbaa06d32f047358de4e6a30c28046f4688e", null ],
      [ "Info", "struct_catch_1_1_result_was.html#a3df655c4352b2a8f892113ec2324ebbaa1cd805eaf0bb58a90fe7e7e4cf6a3cdc", null ],
      [ "Warning", "struct_catch_1_1_result_was.html#a3df655c4352b2a8f892113ec2324ebbaa48f2bb70fceb692a2dedd8cea496c44b", null ],
      [ "FailureBit", "struct_catch_1_1_result_was.html#a3df655c4352b2a8f892113ec2324ebbaa2a771444abe1d6c36859a5986eb4eb59", null ],
      [ "ExpressionFailed", "struct_catch_1_1_result_was.html#a3df655c4352b2a8f892113ec2324ebbaad196f293906219128dbbdcdddba5245b", null ],
      [ "ExplicitFailure", "struct_catch_1_1_result_was.html#a3df655c4352b2a8f892113ec2324ebbaa4d37b01bc48e760e3e50a51357eb98be", null ],
      [ "Exception", "struct_catch_1_1_result_was.html#a3df655c4352b2a8f892113ec2324ebbaa17524a77b7673b0322cc15aa590d1e41", null ],
      [ "ThrewException", "struct_catch_1_1_result_was.html#a3df655c4352b2a8f892113ec2324ebbaa3c99b9157c64302c5d21fe72d7a5fef3", null ],
      [ "DidntThrowException", "struct_catch_1_1_result_was.html#a3df655c4352b2a8f892113ec2324ebbaa60648c2ffff62661a7e904c90f9d2c80", null ],
      [ "FatalErrorCondition", "struct_catch_1_1_result_was.html#a3df655c4352b2a8f892113ec2324ebbaac681dac676e086a60e902b68bdfa6d00", null ]
    ] ]
];