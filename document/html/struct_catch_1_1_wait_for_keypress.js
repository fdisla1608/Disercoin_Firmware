var struct_catch_1_1_wait_for_keypress =
[
    [ "When", "struct_catch_1_1_wait_for_keypress.html#a7d8cf541190124fad592b314fc8849b1", [
      [ "Never", "struct_catch_1_1_wait_for_keypress.html#a7d8cf541190124fad592b314fc8849b1a7e83d9961aaf0904a80ba6bbfd6c4bcc", null ],
      [ "BeforeStart", "struct_catch_1_1_wait_for_keypress.html#a7d8cf541190124fad592b314fc8849b1a2418f85f445875f88f5bae787187c7fe", null ],
      [ "BeforeExit", "struct_catch_1_1_wait_for_keypress.html#a7d8cf541190124fad592b314fc8849b1a3495ae39e4e1c5d7d8c78372c90d44f8", null ],
      [ "BeforeStartAndExit", "struct_catch_1_1_wait_for_keypress.html#a7d8cf541190124fad592b314fc8849b1ac11fc6acd9e4317505a21f536ecb3f21", null ]
    ] ]
];