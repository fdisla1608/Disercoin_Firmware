var namespaces_dup =
[
    [ "Module", "namespace_module.html", "namespace_module" ],
    [ "Network", "namespace_network.html", "namespace_network" ],
    [ "POG", "namespace_p_o_g.html", [
      [ "addToCoinReceive", "namespace_p_o_g.html#a4fd6feee932ae2735724cb8e38791f43", null ],
      [ "BlinkLedIN", "namespace_p_o_g.html#aa1d0e4fbeb621e93acb4a9ce295ba093", null ],
      [ "BlinkLedOUT", "namespace_p_o_g.html#ad8b51b771b2b950ab589b55838742099", null ],
      [ "GetPOGStatus", "namespace_p_o_g.html#a442eaa8a358a13193ff8144a004da126", null ],
      [ "POGPoll", "namespace_p_o_g.html#a949a6b0144bbef033b0511c25f9698c1", null ],
      [ "sendCoinPOG", "namespace_p_o_g.html#af922d51928356cb9966f5f8e96552be4", null ],
      [ "SetCashout", "namespace_p_o_g.html#a789d743c9dc9e6ded9428605fccccdd7", null ],
      [ "SetDailyTicket", "namespace_p_o_g.html#a66223156e5eed1e45867d8df746a9255", null ],
      [ "SetOutServices", "namespace_p_o_g.html#a56129fa3ac7d39b7fd6cb6a7ac4d686f", null ],
      [ "setPOGStatus", "namespace_p_o_g.html#a28b32125cda77b83e041607bb5688664", null ],
      [ "SetResetError", "namespace_p_o_g.html#a5e6b0876af9c6e3ea3655659b6f067da", null ]
    ] ]
];