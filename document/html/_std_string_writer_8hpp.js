var _std_string_writer_8hpp =
[
    [ "is_std_string< T, typename >", "structis__std__string.html", null ],
    [ "is_std_string< T, typename enable_if< is_same< void, decltype(T().push_back( 'a'))>::value &&is_same< T &, decltype(T().append(\"\"))>::value >::type >", "structis__std__string_3_01_t_00_01typename_01enable__if_3_01is__same_3_01void_00_01decltype_07_t0a80ef5ccad6aa645af674bb52b9f0c6.html", null ],
    [ "Writer< TDestination, typename enable_if< is_std_string< TDestination >::value >::type >", "class_writer_3_01_t_destination_00_01typename_01enable__if_3_01is__std__string_3_01_t_destinatiof6be8983fac4e1c0f303b551b7abba99.html", "class_writer_3_01_t_destination_00_01typename_01enable__if_3_01is__std__string_3_01_t_destinatiof6be8983fac4e1c0f303b551b7abba99" ]
];