var namespacetestcases =
[
    [ "mqtt_basic", "namespacetestcases_1_1mqtt__basic.html", "namespacetestcases_1_1mqtt__basic" ],
    [ "mqtt_publish_in_callback", "namespacetestcases_1_1mqtt__publish__in__callback.html", "namespacetestcases_1_1mqtt__publish__in__callback" ],
    [ "settings", "namespacetestcases_1_1settings.html", [
      [ "arduino_ip", "namespacetestcases_1_1settings.html#a5f0cc70d60dc6bbed352a7933ec72323", null ],
      [ "server_ip", "namespacetestcases_1_1settings.html#a2b07e2a6a1722ed553f94efae0e5a73d", null ]
    ] ]
];