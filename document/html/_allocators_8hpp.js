var _allocators_8hpp =
[
    [ "FailingAllocator", "struct_failing_allocator.html", null ],
    [ "AllocatorLogEntry", "class_allocator_log_entry.html", "class_allocator_log_entry" ],
    [ "AllocatorLog", "class_allocator_log.html", "class_allocator_log" ],
    [ "SpyingAllocator", "class_spying_allocator.html", "class_spying_allocator" ],
    [ "KillswitchAllocator", "class_killswitch_allocator.html", "class_killswitch_allocator" ],
    [ "TimebombAllocator", "class_timebomb_allocator.html", "class_timebomb_allocator" ],
    [ "Allocate", "_allocators_8hpp.html#a0aed2ff05f17463e0001268151451b24", null ],
    [ "AllocateFail", "_allocators_8hpp.html#a51b3196bb5f5f92c5145fb332c9754e4", null ],
    [ "Deallocate", "_allocators_8hpp.html#aeafd8fc89367bfb0b199fc643d9630fa", null ],
    [ "Reallocate", "_allocators_8hpp.html#aa2afe8caf7fca8fdb37797605de41cc5", null ],
    [ "ReallocateFail", "_allocators_8hpp.html#a5a59d0927a1410fb91b2863d4d8f3252", null ],
    [ "sizeofPool", "_allocators_8hpp.html#a21f4754274aac72290f4c43906f94ae8", null ],
    [ "sizeofPoolList", "_allocators_8hpp.html#a982233c816f92197f1401c4a5d9a4bdd", null ],
    [ "sizeofString", "_allocators_8hpp.html#a04b6922cb5e6a374456997e987b3e9f4", null ],
    [ "sizeofStringBuffer", "_allocators_8hpp.html#aaa3eed719ac852a27aba4be453ffeb44", null ]
];