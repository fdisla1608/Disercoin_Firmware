var class_collection_iterator =
[
    [ "CollectionIterator", "class_collection_iterator.html#a714687d5ba94abac6c48c6d8fa88c641", null ],
    [ "data", "class_collection_iterator.html#a807ef04d72c6bc2408b150c811cdff58", null ],
    [ "data", "class_collection_iterator.html#a879c842e94f60ecc50a7946d39a18ef3", null ],
    [ "done", "class_collection_iterator.html#a53696257abcf958af60783f641fd9618", null ],
    [ "key", "class_collection_iterator.html#a766a2d60e2b34b871ef42bcc3ef7ef8b", null ],
    [ "next", "class_collection_iterator.html#ac41a48c8479f5f565440ff4525daff37", null ],
    [ "operator!=", "class_collection_iterator.html#a913d271f9fcc1d5be5786329c515fa73", null ],
    [ "operator*", "class_collection_iterator.html#ade6e6033e58686c9fbc632b6f8430fee", null ],
    [ "operator*", "class_collection_iterator.html#af463b4c98656a409c1aab0d3a523e516", null ],
    [ "operator->", "class_collection_iterator.html#a1824b96b07299ae90d8c9156ef65f773", null ],
    [ "operator==", "class_collection_iterator.html#af0959995f98f71f405cb3dbafded52c2", null ],
    [ "ownsKey", "class_collection_iterator.html#ad85b2a3fe7366de2d8521062cd555194", null ],
    [ "setKey", "class_collection_iterator.html#a70a8f16170454154547cd019a156c97a", null ],
    [ "setKey", "class_collection_iterator.html#a0277d3844e4bb19729c5ebcc76db0a0f", null ],
    [ "CollectionData", "class_collection_iterator.html#a988d04fd14e5d947855348620dab5bee", null ]
];