var searchData=
[
  ['serverhost_0',['serverHost',['../struct_module_info.html#afbb5f7127f929899b9d44652e65b21c5',1,'ModuleInfo']]],
  ['serverhost_1',['SERVERHOST',['../_config_8hpp.html#afec51a0ce7435b6bc03fbee42fcf3891',1,'Config.hpp']]],
  ['serverport_2',['SERVERPORT',['../_config_8hpp.html#a40978b1f601fcbf30974d7ad722803a4',1,'Config.hpp']]],
  ['spanish_3',['spanish',['../struct_event_dictionary.html#a1ca5762194a0984c2efcb44b1c86138e',1,'EventDictionary']]],
  ['state_4',['state',['../struct_ticket_info.html#ab30ba07e2a0bd07a15e45a92c32db9c5',1,'TicketInfo']]],
  ['staticip_5',['staticIp',['../struct_module_info.html#aa03cabd7db3b06cc5d54ce6da0e5b4c0',1,'ModuleInfo']]],
  ['subnetmask_6',['subNetMask',['../struct_module_info.html#a9dc1d781325516ec40ed2ffe13408e56',1,'ModuleInfo']]]
];
