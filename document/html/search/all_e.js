var searchData=
[
  ['sas_5faddress_0',['SAS_ADDRESS',['../_config_8hpp.html#af26a5dd9756ee9bc16893a47e52fd832',1,'Config.hpp']]],
  ['sas_5frx_1',['SAS_RX',['../_config_8hpp.html#abf330ee9473c64af68ff4b792a29e70c',1,'Config.hpp']]],
  ['sas_5ftx_2',['SAS_TX',['../_config_8hpp.html#a6d6c11c05faaea4e6100f27c47aab6de',1,'Config.hpp']]],
  ['savelog_3',['SaveLog',['../namespace_module.html#a3b6571c89a65aa72beb37afef06086f6',1,'Module']]],
  ['sendcoinpog_4',['sendCoinPOG',['../namespace_p_o_g.html#af922d51928356cb9966f5f8e96552be4',1,'POG']]],
  ['serverhost_5',['serverHost',['../struct_module_info.html#afbb5f7127f929899b9d44652e65b21c5',1,'ModuleInfo']]],
  ['serverhost_6',['SERVERHOST',['../_config_8hpp.html#afec51a0ce7435b6bc03fbee42fcf3891',1,'Config.hpp']]],
  ['serverport_7',['SERVERPORT',['../_config_8hpp.html#a40978b1f601fcbf30974d7ad722803a4',1,'Config.hpp']]],
  ['setcashout_8',['SetCashout',['../namespace_p_o_g.html#a789d743c9dc9e6ded9428605fccccdd7',1,'POG']]],
  ['setcolor_9',['setColor',['../namespace_module.html#acafcec317642ffdaf44a1f0984ddf9d7',1,'Module']]],
  ['setdailyticket_10',['SetDailyTicket',['../namespace_p_o_g.html#a66223156e5eed1e45867d8df746a9255',1,'POG']]],
  ['setenableprint_11',['setEnablePrint',['../namespace_module.html#a6eb5881b8918df4cb8a1a0bfa43e2bda',1,'Module']]],
  ['setoutservices_12',['SetOutServices',['../namespace_p_o_g.html#a56129fa3ac7d39b7fd6cb6a7ac4d686f',1,'POG']]],
  ['setpogstatus_13',['setPOGStatus',['../namespace_p_o_g.html#a28b32125cda77b83e041607bb5688664',1,'POG']]],
  ['setreseterror_14',['SetResetError',['../namespace_p_o_g.html#a5e6b0876af9c6e3ea3655659b6f067da',1,'POG']]],
  ['setrestartmodule_15',['setRestartModule',['../namespace_module.html#a185f04129a8737c331e02c4c554520a1',1,'Module']]],
  ['setupdatemodule_16',['setUpdateModule',['../namespace_module.html#a4a0d0c7ca8b4a4cc40fa15de0e269595',1,'Module']]],
  ['spanish_17',['spanish',['../struct_event_dictionary.html#a1ca5762194a0984c2efcb44b1c86138e',1,'EventDictionary']]],
  ['standbye_5fserver_18',['STANDBYE_SERVER',['../_config_8hpp.html#a9f12a41cded1b13127e6e1c3a53d89f5',1,'Config.hpp']]],
  ['state_19',['state',['../struct_ticket_info.html#ab30ba07e2a0bd07a15e45a92c32db9c5',1,'TicketInfo']]],
  ['staticip_20',['staticIp',['../struct_module_info.html#aa03cabd7db3b06cc5d54ce6da0e5b4c0',1,'ModuleInfo']]],
  ['subnetmask_21',['subNetMask',['../struct_module_info.html#a9dc1d781325516ec40ed2ffe13408e56',1,'ModuleInfo']]],
  ['subnetmask_22',['SubNetMask',['../main_8hpp.html#aed5db5120c496698b224fbef84211d28',1,'main.hpp']]]
];
