var indexSectionsWithContent =
{
  0: "abcdeghilmnoprstuvwy",
  1: "ehlmpt",
  2: "mnp",
  3: "cemnp",
  4: "abcdeghimnoprsuw",
  5: "acdeghilmnoprstvw",
  6: "o",
  7: "cino",
  8: "bcgilmoprsvwy"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

