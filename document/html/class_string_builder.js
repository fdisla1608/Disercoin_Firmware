var class_string_builder =
[
    [ "StringBuilder", "class_string_builder.html#a2e62aa1e0fd4d330cd18c5979eb57398", null ],
    [ "~StringBuilder", "class_string_builder.html#a4a882aa615fce2f28b21e1813a55c761", null ],
    [ "append", "class_string_builder.html#a8199cdb3536b433bfe5639b1917e9be7", null ],
    [ "append", "class_string_builder.html#a5aef2117c36825289c1c266061336d06", null ],
    [ "append", "class_string_builder.html#aa36bfc0d94a39455034dfc188d2c3dca", null ],
    [ "isValid", "class_string_builder.html#a5bc2a781be2586924afce4e4a4ea6697", null ],
    [ "save", "class_string_builder.html#a97271f36ae8ec62de3812f97c65e5cb6", null ],
    [ "size", "class_string_builder.html#a259cb5a711406a8c3e5d937eb9350cca", null ],
    [ "startString", "class_string_builder.html#a871f09f2337bd19bf89b052bad328027", null ],
    [ "str", "class_string_builder.html#a2601401c62785d9ea8bb00a142823174", null ]
];