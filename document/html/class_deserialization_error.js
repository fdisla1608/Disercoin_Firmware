var class_deserialization_error =
[
    [ "Code", "class_deserialization_error.html#af31477bc48f67856bedb0fa8e5b5281d", [
      [ "Ok", "class_deserialization_error.html#af31477bc48f67856bedb0fa8e5b5281da06d32f047358de4e6a30c28046f4688e", null ],
      [ "EmptyInput", "class_deserialization_error.html#af31477bc48f67856bedb0fa8e5b5281da6a960696ca5356238d587be16dbdf329", null ],
      [ "IncompleteInput", "class_deserialization_error.html#af31477bc48f67856bedb0fa8e5b5281dad235874125ad28b29404ec3fb350ad47", null ],
      [ "InvalidInput", "class_deserialization_error.html#af31477bc48f67856bedb0fa8e5b5281dae1252631a63be5890a1e78ff417d378f", null ],
      [ "NoMemory", "class_deserialization_error.html#af31477bc48f67856bedb0fa8e5b5281dae4dba2e8b0cc1e46c6885406591e338d", null ],
      [ "TooDeep", "class_deserialization_error.html#af31477bc48f67856bedb0fa8e5b5281dafb0290980c82ef92a443a1326940dfff", null ]
    ] ],
    [ "DeserializationError", "class_deserialization_error.html#a116f3635a8c0d44fd31eecb7ca004652", null ],
    [ "DeserializationError", "class_deserialization_error.html#aa5b072c772ed5bd4806fcac4098cb6c9", null ],
    [ "c_str", "class_deserialization_error.html#a03d29d324464cf785ad3f3e51469a5dc", null ],
    [ "code", "class_deserialization_error.html#a9a6d75cdcd83cd2a60e7a71282c1e64d", null ],
    [ "operator bool", "class_deserialization_error.html#a67b76affb3b5d35fa419ac234144038b", null ],
    [ "operator!=", "class_deserialization_error.html#ad6dd04ba35fd152e3999850fa4c2359b", null ],
    [ "operator!=", "class_deserialization_error.html#a162497cf39b2b177e6e69a38f3a61573", null ],
    [ "operator!=", "class_deserialization_error.html#ad14e0b183e9e2d2751681d1b0c1abb11", null ],
    [ "operator==", "class_deserialization_error.html#aaeee7f181e421f3ae9c105895eed46ed", null ],
    [ "operator==", "class_deserialization_error.html#a6060d0c6345e059fd5b39f4324c93ed9", null ],
    [ "operator==", "class_deserialization_error.html#ad72f4ec552a3b04ea43347621f6784b9", null ]
];