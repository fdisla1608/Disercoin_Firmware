var _ram_string_8hpp =
[
    [ "IsChar< T >", "struct_is_char.html", null ],
    [ "ZeroTerminatedRamString", "class_zero_terminated_ram_string.html", "class_zero_terminated_ram_string" ],
    [ "StringAdapter< TChar *, typename enable_if< IsChar< TChar >::value >::type >", "struct_string_adapter_3_01_t_char_01_5_00_01typename_01enable__if_3_01_is_char_3_01_t_char_01_4_1_1value_01_4_1_1type_01_4.html", "struct_string_adapter_3_01_t_char_01_5_00_01typename_01enable__if_3_01_is_char_3_01_t_char_01_4_1_1value_01_4_1_1type_01_4" ],
    [ "StringAdapter< TChar[N], typename enable_if< IsChar< TChar >::value >::type >", "struct_string_adapter_3_01_t_char_0f_n_0e_00_01typename_01enable__if_3_01_is_char_3_01_t_char_01_4_1_1value_01_4_1_1type_01_4.html", "struct_string_adapter_3_01_t_char_0f_n_0e_00_01typename_01enable__if_3_01_is_char_3_01_t_char_01_4_1_1value_01_4_1_1type_01_4" ],
    [ "StaticStringAdapter", "class_static_string_adapter.html", "class_static_string_adapter" ],
    [ "StringAdapter< const char *, void >", "struct_string_adapter_3_01const_01char_01_5_00_01void_01_4.html", "struct_string_adapter_3_01const_01char_01_5_00_01void_01_4" ],
    [ "SizedRamString", "class_sized_ram_string.html", "class_sized_ram_string" ],
    [ "SizedStringAdapter< TChar *, typename enable_if< IsChar< TChar >::value >::type >", "struct_sized_string_adapter_3_01_t_char_01_5_00_01typename_01enable__if_3_01_is_char_3_01_t_char1a0599bc90a3c386cac7bee9eed2eb62.html", "struct_sized_string_adapter_3_01_t_char_01_5_00_01typename_01enable__if_3_01_is_char_3_01_t_char1a0599bc90a3c386cac7bee9eed2eb62" ]
];