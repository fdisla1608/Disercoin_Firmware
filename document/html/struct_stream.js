var struct_stream =
[
    [ "~Stream", "struct_stream.html#a15237b41305f4268e1576c81766cd6d9", null ],
    [ "Stream", "struct_stream.html#a7411b49ed5fda5181dd182d64984906e", null ],
    [ "error", "struct_stream.html#a91004c239942d94aa16e9d9dc0f45160", null ],
    [ "expect", "struct_stream.html#a64a07f770d10dd33cac6f6ef459a63ed", null ],
    [ "length", "struct_stream.html#ac1a8517b153539bffa03acceb18987a9", null ],
    [ "read", "struct_stream.html#a4afd50731ba321d1b9be909cb288a50b", null ],
    [ "readBytes", "struct_stream.html#abfc7054b3c0513e6c9f0dfac4e1b9b41", null ],
    [ "write", "struct_stream.html#a4921dd89e0f14a9b77dfb3d7960401ca", null ]
];