var class_catch_1_1_string_ref =
[
    [ "const_iterator", "class_catch_1_1_string_ref.html#a4558950bcba7df6bbd307ea07a181f5d", null ],
    [ "size_type", "class_catch_1_1_string_ref.html#af38ce1af42d538f410180c265518d4f0", null ],
    [ "StringRef", "class_catch_1_1_string_ref.html#a048d5583347b59c903171ca89429641d", null ],
    [ "StringRef", "class_catch_1_1_string_ref.html#af55f08bb6413229f4086ab2dc131205c", null ],
    [ "StringRef", "class_catch_1_1_string_ref.html#a5fb28627ada0c388aaa379ed3441eada", null ],
    [ "StringRef", "class_catch_1_1_string_ref.html#a7e51976119d9c376ff148252d7daf92f", null ],
    [ "begin", "class_catch_1_1_string_ref.html#a43564d19bdbc133e4a81cc1a2c002254", null ],
    [ "c_str", "class_catch_1_1_string_ref.html#ac241e6f984b3727471ba4e7cb155de5c", null ],
    [ "data", "class_catch_1_1_string_ref.html#a274ea8daff23ee26369e11ffa37bbb66", null ],
    [ "empty", "class_catch_1_1_string_ref.html#a918aa2db77648f4bd28d098b2291208a", null ],
    [ "end", "class_catch_1_1_string_ref.html#a428cfcc26f33036be7c76ab287ea06c5", null ],
    [ "isNullTerminated", "class_catch_1_1_string_ref.html#abd71166ca95b2e48db46baa3b1a1ca75", null ],
    [ "string", "class_catch_1_1_string_ref.html#a3888dcd59dd5acd1ca5b9bee4c2e252a", null ],
    [ "operator!=", "class_catch_1_1_string_ref.html#a6d3d43294f6360dbb45e08294b249055", null ],
    [ "operator==", "class_catch_1_1_string_ref.html#a3b1c78908831e4e1c7939e4c626d334e", null ],
    [ "operator[]", "class_catch_1_1_string_ref.html#aa4bccf3035e37bdfd15a777cf6a8e9ff", null ],
    [ "size", "class_catch_1_1_string_ref.html#aeda399ad0b26aa2d0116f45b2a4c6ca0", null ],
    [ "substr", "class_catch_1_1_string_ref.html#a63e4620b330bb5f287311afddb0606ca", null ]
];