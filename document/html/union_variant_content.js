var union_variant_content =
[
    [ "VariantContent", "union_variant_content.html#ab4e76421c4d6d284ba7469268bcdf0ac", null ],
    [ "asArray", "union_variant_content.html#a396bf83f295d0795dca38a79fb113f30", null ],
    [ "asBoolean", "union_variant_content.html#a631e988db47478461a579db29c0435ac", null ],
    [ "asCollection", "union_variant_content.html#aef287517b6414994e33f1f245906833e", null ],
    [ "asFloat", "union_variant_content.html#a6f0831122085c018c88fef0ffe338644", null ],
    [ "asLinkedString", "union_variant_content.html#adfad9384561ad2c7380d886a79befccf", null ],
    [ "asObject", "union_variant_content.html#aba9eff1e35d61bcaaaf5e8e924e916a9", null ],
    [ "asOwnedString", "union_variant_content.html#aeeb34aa333204c62648e61b0d991d00f", null ],
    [ "asSignedInteger", "union_variant_content.html#a09445371f16117a02cf95894aa8f83db", null ],
    [ "asUnsignedInteger", "union_variant_content.html#ac876b90d42655a1f1e91fa9cb7250631", null ]
];