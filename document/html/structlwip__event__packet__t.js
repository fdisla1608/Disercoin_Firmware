var structlwip__event__packet__t =
[
    [ "accept", "structlwip__event__packet__t.html#ac725c31c64ddb9f8cd5dbddcbcc52bdf", null ],
    [ "addr", "structlwip__event__packet__t.html#a85f0ef6d34c8bb6c193e0a22f8dadfcf", null ],
    [ "arg", "structlwip__event__packet__t.html#a9ce2ec4812a92cb6ab39f6e81e9173a9", null ],
    [ "client", "structlwip__event__packet__t.html#a9c6eedd7da41b83e07aacb1526e7b0ee", null ],
    [ "connected", "structlwip__event__packet__t.html#a15d5c64552c4d2e0144ca60d64f6aa9a", null ],
    [ "dns", "structlwip__event__packet__t.html#aec786dedfc440040a5e8b8af5224fc03", null ],
    [ "err", "structlwip__event__packet__t.html#ac0decc5a3bfbe91fe04533d8a959822e", null ],
    [ "error", "structlwip__event__packet__t.html#af5f3b30d38d3ad67cb14200953b93c15", null ],
    [ "event", "structlwip__event__packet__t.html#a0d8861c8fff7efe9fa23102f30366215", null ],
    [ "fin", "structlwip__event__packet__t.html#a272f24ca800c4d2054e0270c9e3ff010", null ],
    [ "len", "structlwip__event__packet__t.html#a8aed22e2c7b283705ec82e0120515618", null ],
    [ "name", "structlwip__event__packet__t.html#a8f8f80d37794cde9472343e4487ba3eb", null ],
    [ "pb", "structlwip__event__packet__t.html#a120f92299150906118ee7768b6307930", null ],
    [ "pcb", "structlwip__event__packet__t.html#a37d9eb1674e8b4e5e753587acb37b8f6", null ],
    [ "pcb", "structlwip__event__packet__t.html#ab9cd042eac79cdafe66436c6c048ba57", null ],
    [ "poll", "structlwip__event__packet__t.html#aa15a8b31b11dd2e7f7bf87e4ecfa9ca7", null ],
    [ "recv", "structlwip__event__packet__t.html#a9588179fbf0fefef4eeb449818b474ee", null ],
    [ "sent", "structlwip__event__packet__t.html#adedfe4455c16a482aaec054a1ef47fe1", null ]
];