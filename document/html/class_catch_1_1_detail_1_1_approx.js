var class_catch_1_1_detail_1_1_approx =
[
    [ "Approx", "class_catch_1_1_detail_1_1_approx.html#aceee5183add1ad2ad9556a242957945f", null ],
    [ "Approx", "class_catch_1_1_detail_1_1_approx.html#a3aaf1973dfcb97b6ea6eb5df93bdefa8", null ],
    [ "epsilon", "class_catch_1_1_detail_1_1_approx.html#a55d2b4b0f0f68877d7b07a179f08aef4", null ],
    [ "margin", "class_catch_1_1_detail_1_1_approx.html#a867d1bd62e7868d61077c188e4d1082b", null ],
    [ "operator()", "class_catch_1_1_detail_1_1_approx.html#a501e0e5830a0dcf3238b1c2bbb1396b5", null ],
    [ "operator-", "class_catch_1_1_detail_1_1_approx.html#a335be90ddad3692b668a658705fc8b1a", null ],
    [ "scale", "class_catch_1_1_detail_1_1_approx.html#a4037891c5cd15e34caab4269c428669e", null ],
    [ "toString", "class_catch_1_1_detail_1_1_approx.html#a1fe5121d6528fdea3f243321b3fa3a49", null ],
    [ "operator!=", "class_catch_1_1_detail_1_1_approx.html#a31d62e3c35abb86cf25e02601966ca5d", null ],
    [ "operator!=", "class_catch_1_1_detail_1_1_approx.html#a29696f14ebd51887c8c88e771d12ef54", null ],
    [ "operator<=", "class_catch_1_1_detail_1_1_approx.html#a6040b908588745570847d7ae8483b091", null ],
    [ "operator<=", "class_catch_1_1_detail_1_1_approx.html#a0369de03e81bc2ceaf6c9d830476bd49", null ],
    [ "operator==", "class_catch_1_1_detail_1_1_approx.html#a0e5ef1957d4c38d7857005909c613743", null ],
    [ "operator==", "class_catch_1_1_detail_1_1_approx.html#ab38782a37d09b527ca5e126dbf433dda", null ],
    [ "operator>=", "class_catch_1_1_detail_1_1_approx.html#a5899b8a36725406701e2ebded2971ee6", null ],
    [ "operator>=", "class_catch_1_1_detail_1_1_approx.html#affd27efc62be386daeecb7a09e828d44", null ]
];