var class_variant_pool =
[
    [ "allocSlot", "class_variant_pool.html#ac8c0cff77af800cef6e8d5651773b5c8", null ],
    [ "clear", "class_variant_pool.html#ac8bb3912a3ce86b15842e79d0b421204", null ],
    [ "create", "class_variant_pool.html#a1e54fa2eb1210235a29e0830cfcf7792", null ],
    [ "destroy", "class_variant_pool.html#aa7a11a8f73245de26086b0d32c96e719", null ],
    [ "getSlot", "class_variant_pool.html#a12a314e4e43afb3146f8836d1ecbede8", null ],
    [ "shrinkToFit", "class_variant_pool.html#aa1b30144fab64acd36dff39993824402", null ],
    [ "usage", "class_variant_pool.html#a970c5cb0bf579bb88b2077fbc0e4d704", null ]
];