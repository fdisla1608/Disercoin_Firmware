var class_json_string =
[
    [ "Ownership", "class_json_string.html#a7fc5ad23d8bdbc2ef84b60aa5b90d0e4", [
      [ "Copied", "class_json_string.html#a7fc5ad23d8bdbc2ef84b60aa5b90d0e4a546dc4fb82dfcc576436a1a1d1ca8c78", null ],
      [ "Linked", "class_json_string.html#a7fc5ad23d8bdbc2ef84b60aa5b90d0e4af1439db3469f2684bf6430025f5b2a7c", null ]
    ] ],
    [ "JsonString", "class_json_string.html#ad48a051fe453069270ded893ce5b6f18", null ],
    [ "JsonString", "class_json_string.html#ab0133289b71d43decb27be9de5beb76f", null ],
    [ "JsonString", "class_json_string.html#a4b5bd1ff217b5cd93cd5218a9cd21fbb", null ],
    [ "c_str", "class_json_string.html#a03d29d324464cf785ad3f3e51469a5dc", null ],
    [ "isLinked", "class_json_string.html#a768b6250dcd110d2bd35db4c5b528c13", null ],
    [ "isNull", "class_json_string.html#abada6dfb33f4cbafe1e443a5cf8dc8d0", null ],
    [ "operator bool", "class_json_string.html#a67b76affb3b5d35fa419ac234144038b", null ],
    [ "size", "class_json_string.html#a259cb5a711406a8c3e5d937eb9350cca", null ],
    [ "operator!=", "class_json_string.html#aae0c13553209c8b4504caa6730509a41", null ],
    [ "operator==", "class_json_string.html#ae8b51dfae07db37d96b5838bce831322", null ]
];