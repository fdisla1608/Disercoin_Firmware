var class_client =
[
    [ "available", "class_client.html#aebd60457902debb30b07971a16f24ebd", null ],
    [ "connect", "class_client.html#a475258d5bda463bac8ac60d391377e34", null ],
    [ "connect", "class_client.html#a7b5d23b2df67ab5f84971100f1f9e825", null ],
    [ "connected", "class_client.html#a4da62bf6f27e3c10bc4f7b2d92dca244", null ],
    [ "flush", "class_client.html#a50ab71f4bc571f6e246b20db4b3dd131", null ],
    [ "operator bool", "class_client.html#ac496133545cba767b45c6ce8df3587e7", null ],
    [ "peek", "class_client.html#a9ae768d427519818aa552adf467bf65a", null ],
    [ "read", "class_client.html#a4afd50731ba321d1b9be909cb288a50b", null ],
    [ "read", "class_client.html#a699f8a687f8e4ab1c08cc4ea1da121bc", null ],
    [ "stop", "class_client.html#a0efff8623a2fb79dad94a96dcf16d966", null ],
    [ "write", "class_client.html#a7565f7448952b08e42489b3162638f69", null ],
    [ "write", "class_client.html#acde4db2f92186810af3493fd2c7535f0", null ]
];