var class_variant_pool_list =
[
    [ "VariantPoolList", "class_variant_pool_list.html#a2e129a7acefd504c7e429299b3b3af49", null ],
    [ "~VariantPoolList", "class_variant_pool_list.html#a2698cf61bc9049fe96f983dd858301d0", null ],
    [ "allocSlot", "class_variant_pool_list.html#a4d17d5878c3ef87928043d8ca5b34e6a", null ],
    [ "clear", "class_variant_pool_list.html#a9665a1ab42dc24272e1954f2014c6b7a", null ],
    [ "freeSlot", "class_variant_pool_list.html#a03bb02c259650f83d677f8233355e798", null ],
    [ "getSlot", "class_variant_pool_list.html#a12a314e4e43afb3146f8836d1ecbede8", null ],
    [ "operator=", "class_variant_pool_list.html#a2604fdfdb8171c3d60acef357f7e323d", null ],
    [ "shrinkToFit", "class_variant_pool_list.html#a15972c20a445684589b43f6c45defaff", null ],
    [ "usage", "class_variant_pool_list.html#a970c5cb0bf579bb88b2077fbc0e4d704", null ],
    [ "swap", "class_variant_pool_list.html#a1ca1af7a14269c70052e07f0519e89c7", null ]
];