var structtcp__api__call__t =
[
    [ "addr", "structtcp__api__call__t.html#a0e68523738a4212dc6a771579ca9df76", null ],
    [ "apiflags", "structtcp__api__call__t.html#aa7d5789e3058870e04b830f284808c0d", null ],
    [ "backlog", "structtcp__api__call__t.html#ab465926c595db8f5b502f8711ef456e6", null ],
    [ "bind", "structtcp__api__call__t.html#ac8f9338a281cb44b8ac75a792fd75b19", null ],
    [ "call", "structtcp__api__call__t.html#a9654431cab60b50c27a25be581aff8d1", null ],
    [ "cb", "structtcp__api__call__t.html#a8d1d08a8abc3fb923a9a214b2323f59a", null ],
    [ "closed_slot", "structtcp__api__call__t.html#a82ee962927ca4f9c3ca13596f8c47080", null ],
    [ "connect", "structtcp__api__call__t.html#a0d37301a4931185ff88ad070d14cf70e", null ],
    [ "data", "structtcp__api__call__t.html#a8f64897c7ccc5c13f276d1d07c4e7095", null ],
    [ "err", "structtcp__api__call__t.html#ac0decc5a3bfbe91fe04533d8a959822e", null ],
    [ "pcb", "structtcp__api__call__t.html#ab9cd042eac79cdafe66436c6c048ba57", null ],
    [ "port", "structtcp__api__call__t.html#a8e0798404bf2cf5dabb84c5ba9a4f236", null ],
    [ "received", "structtcp__api__call__t.html#aed62e6066fba34601c7422c0dd150fc2", null ],
    [ "size", "structtcp__api__call__t.html#a854352f53b148adc24983a58a1866d66", null ],
    [ "write", "structtcp__api__call__t.html#a3d00c9935eeb576da00d24883b139b56", null ]
];