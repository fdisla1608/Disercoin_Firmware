var annotated_dup =
[
    [ "Module", "namespace_module.html", [
      [ "Log", "struct_module_1_1_log.html", "struct_module_1_1_log" ]
    ] ],
    [ "Network", "namespace_network.html", [
      [ "HttpResponse", "struct_network_1_1_http_response.html", "struct_network_1_1_http_response" ]
    ] ],
    [ "ErrState", "struct_err_state.html", "struct_err_state" ],
    [ "EventDictionary", "struct_event_dictionary.html", "struct_event_dictionary" ],
    [ "ModuleInfo", "struct_module_info.html", "struct_module_info" ],
    [ "pulses_t", "structpulses__t.html", "structpulses__t" ],
    [ "TicketInfo", "struct_ticket_info.html", "struct_ticket_info" ]
];