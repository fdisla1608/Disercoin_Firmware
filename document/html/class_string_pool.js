var class_string_pool =
[
    [ "StringPool", "class_string_pool.html#aa035f928c3ca1f75049551fbf35d0039", null ],
    [ "StringPool", "class_string_pool.html#a6ff475a9c19c8e638a42f35edbb4b162", null ],
    [ "~StringPool", "class_string_pool.html#ac30891490c547b0d1ab471f7964ffe3b", null ],
    [ "add", "class_string_pool.html#a3d4751a19bba1637e488b866fba63ea3", null ],
    [ "add", "class_string_pool.html#a32dea86e7556daebe06293887c961218", null ],
    [ "clear", "class_string_pool.html#a9665a1ab42dc24272e1954f2014c6b7a", null ],
    [ "dereference", "class_string_pool.html#a5a88ed7b7e24b5e66f6e6ff0ac82828c", null ],
    [ "get", "class_string_pool.html#ab3bbc94d511f7e5eb04cb3e3c92d5204", null ],
    [ "operator=", "class_string_pool.html#a7d757f97c31ca2f6023a3f03f136c489", null ],
    [ "size", "class_string_pool.html#a259cb5a711406a8c3e5d937eb9350cca", null ],
    [ "swap", "class_string_pool.html#a4777a84d28bdb171a5fc8fb198bdfc42", null ]
];