var _p_o_g_8hpp =
[
    [ "addToCoinReceive", "_p_o_g_8hpp.html#a4fd6feee932ae2735724cb8e38791f43", null ],
    [ "BlinkLedIN", "_p_o_g_8hpp.html#aa1d0e4fbeb621e93acb4a9ce295ba093", null ],
    [ "BlinkLedOUT", "_p_o_g_8hpp.html#ad8b51b771b2b950ab589b55838742099", null ],
    [ "GetPOGStatus", "_p_o_g_8hpp.html#a442eaa8a358a13193ff8144a004da126", null ],
    [ "POGPoll", "_p_o_g_8hpp.html#a949a6b0144bbef033b0511c25f9698c1", null ],
    [ "sendCoinPOG", "_p_o_g_8hpp.html#af922d51928356cb9966f5f8e96552be4", null ],
    [ "SetCashout", "_p_o_g_8hpp.html#a789d743c9dc9e6ded9428605fccccdd7", null ],
    [ "SetDailyTicket", "_p_o_g_8hpp.html#a66223156e5eed1e45867d8df746a9255", null ],
    [ "SetOutServices", "_p_o_g_8hpp.html#a56129fa3ac7d39b7fd6cb6a7ac4d686f", null ],
    [ "setPOGStatus", "_p_o_g_8hpp.html#a28b32125cda77b83e041607bb5688664", null ],
    [ "SetResetError", "_p_o_g_8hpp.html#a5e6b0876af9c6e3ea3655659b6f067da", null ]
];