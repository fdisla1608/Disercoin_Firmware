var class_spying_allocator =
[
    [ "SpyingAllocator", "class_spying_allocator.html#a79bbc47b82b33eb737a94f59e88b6a19", null ],
    [ "~SpyingAllocator", "class_spying_allocator.html#abcfe81f7534cb920f7d56d32c4a54d2f", null ],
    [ "allocate", "class_spying_allocator.html#a9a47d0e5bd7f6b8d2b11ff88b9e85242", null ],
    [ "allocatedBytes", "class_spying_allocator.html#aaee7af7441505c7b9e49ade91c05d3fe", null ],
    [ "clearLog", "class_spying_allocator.html#ae2d5212eb62104cb3219318f03f23734", null ],
    [ "deallocate", "class_spying_allocator.html#a6a642f26b25d656d8c8ad0762d07984c", null ],
    [ "log", "class_spying_allocator.html#af00e95d67badaa84f3ed28f31830c6a9", null ],
    [ "reallocate", "class_spying_allocator.html#aba8324039501bba3df9addfb1410cfc4", null ]
];