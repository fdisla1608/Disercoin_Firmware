var class_collection_data =
[
    [ "iterator", "class_collection_data.html#a7ea6a2b642bb85af897a3c9fbde2bc7c", null ],
    [ "addSlot", "class_collection_data.html#afc73baa61294e212bc585503ee2f3faf", null ],
    [ "clear", "class_collection_data.html#a55f5a16731e67b42497235b00ac939dd", null ],
    [ "createIterator", "class_collection_data.html#a0a0480472e177807c0dcd5840a3fc791", null ],
    [ "head", "class_collection_data.html#a39223c08f2134fe3f9e5185f389208d5", null ],
    [ "nesting", "class_collection_data.html#acf0102aca8b897be96bd3d3965541e24", null ],
    [ "remove", "class_collection_data.html#ae031b2c1eec5ce462770cf111ec47c01", null ],
    [ "size", "class_collection_data.html#aa0b007c53124b9d9088f05d996e505cd", null ]
];