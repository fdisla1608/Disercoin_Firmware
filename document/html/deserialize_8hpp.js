var deserialize_8hpp =
[
    [ "first_or_void<... >", "structfirst__or__void.html", "structfirst__or__void" ],
    [ "first_or_void< T, Rest... >", "structfirst__or__void_3_01_t_00_01_rest_8_8_8_01_4.html", "structfirst__or__void_3_01_t_00_01_rest_8_8_8_01_4" ],
    [ "is_deserialize_destination< T, class >", "structis__deserialize__destination.html", null ],
    [ "is_deserialize_destination< T, typename enable_if< is_same< decltype(VariantAttorney::getResourceManager(detail::declval< T & >())), ResourceManager * >::value >::type >", "structis__deserialize__destination_3_01_t_00_01typename_01enable__if_3_01is__same_3_01decltype_083e7a23c8bfc80b410173a7dd2bcbafe.html", null ],
    [ "deserialize", "deserialize_8hpp.html#aacd30a2f28250257190025366f603ddd", null ],
    [ "deserialize", "deserialize_8hpp.html#ae83a39266f61cbd8220cd25ba6a6f36e", null ],
    [ "doDeserialize", "deserialize_8hpp.html#a4b18f81a005e0382951a603343b882ce", null ],
    [ "shrinkJsonDocument", "deserialize_8hpp.html#a81d9e96f35caa20deb7b95007eaa7de2", null ]
];