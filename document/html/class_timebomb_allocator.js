var class_timebomb_allocator =
[
    [ "TimebombAllocator", "class_timebomb_allocator.html#a172c142958b8d40ada491fe666623525", null ],
    [ "~TimebombAllocator", "class_timebomb_allocator.html#a9171e7f910a140ad7f8827cc2f58b792", null ],
    [ "allocate", "class_timebomb_allocator.html#a9a47d0e5bd7f6b8d2b11ff88b9e85242", null ],
    [ "deallocate", "class_timebomb_allocator.html#a6a642f26b25d656d8c8ad0762d07984c", null ],
    [ "reallocate", "class_timebomb_allocator.html#a523e1f7f9c51092169a94d9d0b34954d", null ],
    [ "setCountdown", "class_timebomb_allocator.html#a0b20efb9a666262c6306b058ef94d39b", null ]
];