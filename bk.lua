#include "Network.hpp"

AsyncWebServer server(80);

namespace Network
{

  WiFiClient client;
  ModuleInfo myModule;
  PubSubClient Pubclient(client);

  bool mqtt_initialized = false;
  char logString[200];
  uint32_t timerReconnect = 0;

  void Initialize(ModuleInfo myModuleObj)
  {
    myModule = myModuleObj;
    Pubclient.setServer(IPAddress(70, 35, 199, 64), myModuleObj.mqttPort);
    Pubclient.setCallback(callback);
  }

  // HTTP
  uint8_t NetworkConnected()
  {
    if (WiFiGenericClass::getStatusBits() & STA_HAS_IP_BIT)
    {
      return 1;
    }
    else if ((WiFiGenericClass::getStatusBits() & ETH_HAS_IP_BIT))
    {
      return 2;
    }
    else
    {
      return 0;
    }
  }
  bool MQTTConnected()
  {
    return Pubclient.connected();
  }

  bool MQTTPublish(const char *topic, const char *payload)
  {
    return Pubclient.publish(topic, payload);
  }

  HttpResponse HTTPPOST(String url, String payload)
  {
    HttpResponse request;
    HTTPClient http;
    http.begin(url);
    http.addHeader("Content-Type", "application/json");
    int httpCode = http.POST(payload);
    if (httpCode > 0)
    {
      request.responseCode = httpCode;
      request.payload = http.getString();
    }
    else
    {
      request.responseCode = httpCode;
      request.payload = http.errorToString(httpCode);
    }
    http.end();
    return request;
  }
  HttpResponse HTTPGET(String url)
  {
    HttpResponse request;
    if (1 > 0)
    {
      HTTPClient http;
      http.begin(url);
      int httpCode = http.GET();
      if (httpCode > 0)
      {
        request.responseCode = httpCode;
        request.payload = http.getString();
      }
      else
      {
        request.responseCode = httpCode;
        request.payload = http.errorToString(httpCode);
      }
      http.end();
    }
    else
    {
      request.responseCode = -1;
      request.payload = "No Internet connection";
    }
    return request;
  }

  void POST_AMOUNT(int amount, int creditType)
  {
    JsonDocument httpResponse;
    HttpResponse httpRequest;

    httpResponse["IdModulo"] = myModule.idModulo;
    httpResponse["ValorMoneda"] = myModule.coinValue;

    if (creditType == 1)
    {
      httpResponse["Amount"] = amount;
      String payload = "";
      sprintf(logString, "Credits IN POSTED: %d >> %d\n", amount, Module::CountingCredits("counterIn", amount));
      serializeJson(httpResponse, payload);
      httpRequest = HTTPPOST("http://" + myModule.serverHost + ":" + myModule.httpPort + "/modulos/Entradas", payload);
      Module::MonitoringLog(0x31, logString, __func__, amount);
    }
    else
    {
      httpResponse["Credits"] = amount;
      String payload = "";
      sprintf(logString, "Credits OUT POSTED: %d >> %d\n", amount, Module::CountingCredits("counterOut", amount));
      serializeJson(httpResponse, payload);
      httpRequest = HTTPPOST("http://" + myModule.serverHost + ":" + myModule.httpPort + "/modulos/pulso/Salidas", payload);
      Module::MonitoringLog(0x31, logString, __func__, amount);
    }

    if (httpRequest.responseCode != 200)
    {
      sprintf(logString, "Error al registrar Creditos Parciales, Codigo: %d ", httpRequest.responseCode);
      Module::MonitoringLog(0x54, logString, "onPostingAmount", 0);
    }
  }

  void PostInitialInformation(uint8_t connectionType)
  {
    JsonDocument info;
    String payload;
    info["Modulo"] = Module::GetModuleInformation();
    info["Network"]["HTTP"]["TypeConnection"] = 4;
    // info["Network"]["HTTP"]["IpPublic"] = GetIpPublic();
    info["Network"]["MQTT"]["Topic"] = myModule.mqttTopic;
    info["Network"]["MQTT"]["UserName"] = myModule.mqttUserName;
    info["Network"]["MQTT"]["Password"] = myModule.mqttPassword;
    info["Network"]["MQTT"]["ServerIP"] = myModule.mqttHost;
    info["Network"]["MQTT"]["ServerPort"] = myModule.mqttPort;
    if (connectionType == 7)
    {
      info["Network"]["HTTP"]["TypeConnection"] = "WiFi";
      info["Network"]["HTTP"]["IpLocal"] = WiFi.localIP();
      info["Network"]["HTTP"]["MAC"] = WiFi.macAddress();
      info["Network"]["HTTP"]["SubNetMask"] = WiFi.subnetMask();
      info["Network"]["HTTP"]["GatewayIP"] = WiFi.gatewayIP();
    }
    else
    {
      info["Network"]["HTTP"]["TypeConnection"] = "Ethernet";
      info["Network"]["HTTP"]["IpLocal"] = ETH.localIP();
      info["Network"]["HTTP"]["MAC"] = ETH.macAddress();
      info["Network"]["HTTP"]["SubNetMask"] = ETH.subnetMask();
      info["Network"]["HTTP"]["GatewayIP"] = ETH.gatewayIP();
    }
    serializeJson(info, payload);
    Network::HTTPPOST("http://" + myModule.serverHost + ":" + myModule.httpPort + "/modulos/info", payload);
  }

  void PrintToServer(String printerData)
  {
    JsonDocument data;
    char logString[150];
    String payload = "";
    data["IdModulo"] = myModule.idModulo;
    data["TicketRequest"] = printerData.c_str();
    serializeJson(data, payload);
    HttpResponse httpRequest = HTTPPOST("http://" + myModule.serverHost + ":" + myModule.httpPort + "/modulos/tickets", payload);
    if (httpRequest.responseCode == 200)
    {
      Module::MonitoringLog(0x38, "Ticket Enviado Al Servidor Exitosamente", __func__, 0);
    }
    else
    {
      sprintf(logString, "Error al registrar Ticket, Codigo: %d ", httpRequest.responseCode);
      Module::MonitoringLog(0x71, logString, __func__, httpRequest.responseCode);
    }
  }

  // MQTT
  void MQTTLOOP()
  {
    if (Pubclient.connected())
    {
      Pubclient.loop();
    }
    else
    {
      if (Pubclient.connect(myModule.idModulo.c_str()))
      {
        Module::MonitoringLog(0x57, "MQTT onConnect", __func__, 0);
        Pubclient.subscribe(myModule.mqttTopic.c_str());
      }
      else if (millis() - timerReconnect > 5000)
      {
        Serial.printf("Failed to Connect to MQTT Broker, Code: %d \n", Pubclient.state());
        Serial.println("Try again in 5 seconds");
        timerReconnect = millis();
      }
    }
  }

  void callback(char *topic, byte *payload, unsigned int length)
  {
    JsonDocument dataReceived;
    deserializeJson(dataReceived, payload);
    String idModuloMqtt = dataReceived["idModulo"];
    String commandMqtt = dataReceived["command"];

    if (commandMqtt == "GET_INFORMATION")
    {
      JsonDocument info;
      String informationPayload = "";
      info["Modulo"] = Module::GetModuleInformation();
      info["Bateria"] = Module::GetBatteryStatus();
      info["Puerta"] = Module::GetLdrStatus();
      info["Estado"] = POG::GetPOGStatus();
      info["Counters"]["IN"] = Module::GetCountingCredits("counterIn");
      info["Counters"]["OUT"] = Module::GetCountingCredits("counterOut");
      serializeJson(info, informationPayload);
      Pubclient.publish("trunmore/server", informationPayload.c_str(), informationPayload.length());
    }

    // Module
    if (commandMqtt == "PRINT_ENABLE" && idModuloMqtt == myModule.idModulo)
    {
      Module::setEnablePrint(true);
    }
    if (commandMqtt == "RESTART" && idModuloMqtt == myModule.idModulo)
    {
      Module::setRestartModule(true);
    }
    else if (commandMqtt == "UPDATE" && idModuloMqtt == myModule.idModulo)
    {
      Module::setUpdateModule(true);
    }

    // POG
    else if (commandMqtt == "SEND_MONEY" && idModuloMqtt == myModule.idModulo)
    {
      uint16_t coins = dataReceived["data"]["coin"];
      uint16_t bills = dataReceived["data"]["bills"];
      POG::addToCoinReceive(coins);
    }
    else if (commandMqtt == "RESET" && idModuloMqtt == myModule.idModulo)
    {
      Serial.println("Reset");
      POG::SetResetError(true);
    }
    else if (commandMqtt == "OUT_OF_SERVICE" && idModuloMqtt == myModule.idModulo)
    {
      POG::SetOutServices(true);
    }
    else if (commandMqtt == "DAILY_TICKET" && idModuloMqtt == myModule.idModulo)
    {
      POG::SetDailyTicket(true);
    }
    else if (commandMqtt == "CASHOUT" && idModuloMqtt == myModule.idModulo)
    {
      POG::SetCashout(true);
    }
  }

  void UpdateByOTA()
  {
    WiFiClient client;
    Module::MonitoringLog(0x40, "OTA-Updating........", __func__, 0);
    t_httpUpdate_return ret = httpUpdate.update(client, "http://storage.trunmore.com:8001/WT32/FirmwareWT32.ino.bin");
    switch (ret)
    {
    case HTTP_UPDATE_FAILED:
      sprintf(logString, "HTTP OTA-UPDATE FAILED Error (%d): %s", httpUpdate.getLastError(), httpUpdate.getLastErrorString().c_str());
      Module::MonitoringLog(0x7D, logString, __func__, 0);
      break;

    case HTTP_UPDATE_NO_UPDATES:
      sprintf(logString, "HTTP OTA-UPDATE NO UPDATES");
      Module::MonitoringLog(0x41, logString, __func__, 0);
      break;

    case HTTP_UPDATE_OK:
      sprintf(logString, "HTTP OTA-UPDATE SUCCESSFULLY");
      Module::MonitoringLog(0x42, logString, __func__, 0);
      break;
    }
  }

  // WEB SERVER
  void configureWebServer()
  {
    if (!SPIFFS.begin(true))
    {
      Serial.println("An Error has occurred while mounting SPIFFS");
      return;
    }
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send(SPIFFS, "/index.html", String(), false, processor); });
    server.on("/Success", HTTP_GET, [](AsyncWebServerRequest *request)
              { 
                request->send(SPIFFS, "/SetupComplete.html", String(), false, processor);
                ESP.restart(); });
    server.on("/Pending", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send(SPIFFS, "/SetupPending.html", String(), false, processor); 
                ESP.restart(); });
    server.on("/index.css", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send(SPIFFS, "/index.css", "text/css"); });
    server.on("/index.js", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send(SPIFFS, "/index.js", "application/javascript"); });
    server.on("/logo.png", HTTP_GET, [](AsyncWebServerRequest *request)
              { request->send(SPIFFS, "/logo.png", "image/png"); });

    server.on("/scan", HTTP_GET, [](AsyncWebServerRequest *request)
              {

    int n = WiFi.scanNetworks();
    JsonDocument Device;
    String payload = "";
    Device["Id"] = ESP.getEfuseMac();
    Device["ETH"] = "Connected";

    for (int i = 0; i < n; ++i) {
      Device["Wifi"][i]["SSID"] = WiFi.SSID(i);
      Device["Wifi"][i]["RSSI"] = WiFi.RSSI(i);
      Device["Wifi"]["EncryptionType"] = WiFi.encryptionType(i);
    }
    serializeJson(Device, payload);
    Serial.println("Scan Wifi Complete");
    
    AsyncWebServerResponse *response = request->beginResponse_P(200, "application/json",payload.c_str() );
    response->addHeader("Access-Control-Allow-Origin", "*");
    request->send(response); });

    server.on(
        "/connect", HTTP_POST, [](AsyncWebServerRequest *request) {},
        nullptr,
        Network::onConnectBody);

    server.on(
        "/configure", HTTP_POST, [](AsyncWebServerRequest *request) {},
        nullptr,
        onConfigureBody);

    server.onNotFound(notFound);
    server.onRequestBody(onConnectBody);
    server.onRequestBody(onConfigureBody);
    server.begin();
  }

  void onConnectBody(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total)
  {
    String payload;
    JsonDocument jsonBuffer;
    deserializeJson(jsonBuffer, data);

    String typeConnection = jsonBuffer["typeConnection"];
    String ssid = jsonBuffer["ssid"];
    String password = jsonBuffer["password"];
    IPAddress staticIp;
    IPAddress gtW;
    IPAddress subNetMask;

    if (jsonBuffer.containsKey("ip"))
    {
      staticIp.fromString(jsonBuffer["ip"].as<String>());
    }

    if (jsonBuffer.containsKey("mask"))
    {
      gtW.fromString(jsonBuffer["mask"].as<String>());
    }

    if (jsonBuffer.containsKey("gtw"))
    {
      subNetMask.fromString(jsonBuffer["gtw"].as<String>());
    }

    jsonBuffer.clear();

    if (typeConnection == "WiFi")
    {
      WiFi.begin(ssid.c_str(), password.c_str());
      WiFi.waitForConnectResult(3000);
      if (WiFi.isConnected())
      {
        Serial.println("WiFi Connected!");
        if (staticIp && subNetMask && gtW)
        {
          WiFi.config(staticIp, gtW, subNetMask);
        }
        jsonBuffer["status"] = 1;
        jsonBuffer["description"] = "WiFi Connected!";
      }
      else
      {
        jsonBuffer["status"] = 0;
        jsonBuffer["description"] = "WiFi Disconnected!";
      }
    }
    else if (typeConnection == "Ethernet")
    {
      ETH.begin(ETH_PHY_ADDR, ETH_PHY_POWER);
      if (staticIp && subNetMask && gtW)
      {
        WiFi.config(staticIp, gtW, subNetMask);
      }
      jsonBuffer["status"] = 1;
      jsonBuffer["description"] = "Ethernet Disconnected!";
    }
    else
    {
      jsonBuffer["status"] = 1;
      jsonBuffer["description"] = "Undefined TypeConnection";
    }

    serializeJson(jsonBuffer, payload);
    request->send(200, "text/plain", payload.c_str());
  }
  void onConfigureBody(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total)
  {
    String payload;
    JsonDocument jsonBuffer;
    deserializeJson(jsonBuffer, data);

    String typeConnection = jsonBuffer["typeConnection"];
    String ssid = jsonBuffer["ssid"];
    String password = jsonBuffer["password"];
    IPAddress staticIp;
    IPAddress gtW;
    IPAddress subNetMask;

    if (jsonBuffer.containsKey("ip"))
    {
      staticIp.fromString(jsonBuffer["ip"].as<String>());
    }

    if (jsonBuffer.containsKey("mask"))
    {
      gtW.fromString(jsonBuffer["mask"].as<String>());
    }

    if (jsonBuffer.containsKey("gtw"))
    {
      subNetMask.fromString(jsonBuffer["gtw"].as<String>());
    }

    myModule.typeConnection = typeConnection;
    myModule.WifiSSID = ssid;
    myModule.WifiPassword = password;
    myModule.staticIp = staticIp.toString();
    myModule.subNetMask = subNetMask.toString();
    myModule.gateWay = gtW.toString();
    if (Module::ConfigureModule(myModule))
    {
      request->send(200, "text/plain", "OK");
    }
    else
    {
      request->send(200, "text/plain", "PENDING");
    }
  }
  void notFound(AsyncWebServerRequest *request)
  {
    AsyncWebServerResponse *response = request->beginResponse_P(404, "text/plain", "Not found");
    response->addHeader("Access-Control-Allow-Origin", "*");
    request->send(response);
  }

  String processor(const String &var)
  {
    Serial.println(var);
    return String();
  }

  void WEBServerLoop()
  {
  }
}